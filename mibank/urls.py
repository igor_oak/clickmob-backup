"""URLs da aplicação mibank."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^confirmar/boleto/$', views.confirmar_boleto, name='confirmar_boleto'),
]
