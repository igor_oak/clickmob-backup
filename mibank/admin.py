"""Administração da aplicação mibank."""

from django.contrib import admin

from . import models


@admin.register(models.Boleto)
class BoletoAdmin(admin.ModelAdmin):
    """Boleto na inteface admin."""
