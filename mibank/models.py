"""Modelos da aplicação mibank."""

from django.db import models
from django.utils import timezone

from . import managers


class Boleto(models.Model):
    """Boleto Mibank."""

    valor = models.DecimalField(decimal_places=2, max_digits=9)
    sacado_uf = models.CharField(max_length=2)
    sacado_cep = models.CharField(max_length=10)
    vencimento = models.DateField()
    sacado_nome = models.CharField(max_length=40)
    sacado_email = models.EmailField()
    sacado_bairro = models.CharField(max_length=40)
    sacado_cidade = models.CharField(max_length=40)
    sacado_numero = models.CharField(max_length=10)
    numero_controle = models.IntegerField()
    sacado_documento = models.CharField(max_length=40)
    sacado_logradouro = models.CharField(max_length=255)
    boleto = models.CharField(max_length=255, blank=True, null=True)

    created = models.DateTimeField(default=timezone.now)

    objects = managers.BoletoManager()

    def __str__(self):
        """toString."""
        return '[Controle={}][usuário={}]'.format(self.numero_controle, self.order.owner.username)

    class Meta:
        ordering = ('-created',)
