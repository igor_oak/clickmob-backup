"""Configuração da aplicação mibank."""


from django.apps import AppConfig


class MibankConfig(AppConfig):
    """Configuração da aplicação."""

    name = 'mibank'
