"""Views da aplicação mibank."""

import logging

import requests
from django import http
from django.conf import settings
from orders.models import Order
from rest_framework import views

logger = logging.getLogger(__name__)


class ConfirmarBoletoView(views.APIView):
    """Endpoint para confirmação de boletos, via mibank webhook."""

    def post(self, request, *args, **kwargs):
        """Confirma se o boleto foi realmente pago, atualizando o status do pedido e views do usuário."""
        data = request.data
        logger.info('Recebida notificação do servidor mibank. Dados da notificação: %s', data)

        # essa implementação utiliza os dados do webhook mibank para verificar se realmente o boleto foi pago.
        url = 'https://mibankws.azurewebsites.net/api/boleto/consultar-numero-controle'
        params = dict(chave_api=settings.MIBANK_KEY, numero_controle=data.get('numero_controle', ''))
        response = requests.get(url, params)
        if response.status_code == 200 and response.json()['status'] == 2:  # boleto pago
            try:
                order = Order.objects.get(id=int(data['numero_controle']))
                if order.status != 'Pago':
                    order.confirmar_pagamento()
                    msg = 'Confirmação de pagamento referente ao boleto %s (%s) registrada com sucesso.'
                    logger.info(msg, data['boleto'], data['numero_controle'])

                else:
                    msg = 'Confirmação de pagamento referente ao boleto %s (%s) já foi efetuada.'
                    logger.info(msg, data['boleto'], data['numero_controle'])

            except Order.DoesNotExist as e:
                logger.error('Recebida notificação mibank para boleto não registrado na base de dados. ' +
                             'Dados da notificação: %s', data)

            return http.HttpResponse()

        raise http.Http404()


confirmar_boleto = ConfirmarBoletoView.as_view()
