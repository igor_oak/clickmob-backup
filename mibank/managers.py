"""Managers da aplicação mibank."""

import requests
from django.conf import settings
from django.db import models
from rest_framework import exceptions


class BoletoManager(models.Manager):
    """Gerenciador de boletos."""

    boleto_url = 'https://mibankws.azurewebsites.net/api/boleto/gerar-boletos-cobranca'

    def gerar_boleto(self, data):
        """Cria um boleto via mibank API e sincroniza com o banco de dados da aplicação."""
        _data = data.copy()
        _data['vencimento'] = _data['vencimento'].strftime('%d/%m/%Y')
        # _data['valor'] = float(_data['valor'])

        json_data = dict(
            chave_api=settings.MIBANK_KEY,
            boletos=[_data]
        )
        response = requests.post(self.boleto_url, json=json_data)
        if response.status_code != 200:
            raise exceptions.ValidationError({
                'boleto': response.json()
            })

        response_data = response.json()
        import pprint
        pprint.pprint(response_data)
        data['boleto'] = response_data['boleto']
        return self.model.objects.create(**data)
