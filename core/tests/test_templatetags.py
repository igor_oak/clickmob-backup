"""Testes relacionados a tags e filtros da aplicação core."""

import hashlib
from os import path

from django import template, test
from django.contrib import auth
from django.core.files import uploadedfile
from django.test import client

from ..templatetags import core_tags


class CoreSimpleTagsTests(test.TestCase):
    """Testes de tags simples."""

    @classmethod
    def drop_users(cls):
        """Remove usuários e consequentemente arquivos residuais."""
        for user in auth.get_user_model().objects.all():
            user.delete()

    def tearDown(self):
        """Tear down."""
        self.drop_users()
        super().tearDown()

    @classmethod
    def setUpTestData(cls):
        """Test fixtures."""
        super().setUpTestData()
        cls.drop_users()

        User = auth.get_user_model()

        # este usuário não possui imagem de perfil.
        cls.user = User.objects.create_user('user', 'user@domain.com', 'pass')

        # este usuário possui image de perfil.
        file_path = path.abspath(path.join(path.dirname(__file__), 'fixtures/favicon.ico'))
        with open(file_path, 'rb') as file:
            user_image = uploadedfile.UploadedFile(file, 'user.jpg', 'img/jpg')
            cls.user2 = User.objects.create_user(
                'user2',
                'user2@domain.com',
                'pass',
                user_image=user_image,
                friend=cls.user,
                phone='795879852',
            )

    def test_gravatar_url(self):
        """Verifica se a url gravatar é gerada corretamente em função do email fornecido."""
        email = 'admin@domain.com'
        hash_email = hashlib.md5(email.encode('utf-8')).hexdigest()

        url = core_tags.gravatar_url(email)
        self.assertEqual(url, '//www.gravatar.com/avatar/{}'.format(hash_email))

    def test_user_image(self):
        """Verifica se a imagem do usuário é obtida corretamente."""
        fake_request = client.RequestFactory()
        fake_request.user = self.user

        temp = template.Template('''{% load user_image from core_tags %}{% user_image %}''')

        # neste caso a imagem default é utilizada.
        content = temp.render(template.Context(dict(request=fake_request)))
        self.assertEqual(content, '/static/img/default_avatar.jpeg')

        # neste caso a imagem enviada pelo usuário é utilizada.
        fake_request.user = self.user2
        content = temp.render(template.Context(dict(request=fake_request)))
        self.assertEqual(content, '/media/clickmob/user_images/user2/user.jpg')
