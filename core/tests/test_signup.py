"""Testes dos registro customizado."""

from os import path

from django.contrib import auth
from django.core import urlresolvers
from django.core.files import uploadedfile
from django.test import TestCase
from rest_framework.status import is_success
from rest_framework.test import APITestCase


class SignupTestMixin:
    """Mixin fixtures."""

    @classmethod
    def drop_users(cls):
        """Remove usuários e consequentemente arquivos residuais."""
        for user in auth.get_user_model().objects.all():
            user.delete()

    def tearDown(self):
        """Tear down."""
        self.drop_users()
        super().tearDown()

    @classmethod
    def setUpTestData(cls):
        """Class fixtures."""
        super().setUpTestData()
        cls.drop_users()

        cls.register_url = urlresolvers.reverse('account_signup')
        cls.rest_register_url = urlresolvers.reverse('rest_register')
        cls.User = auth.get_user_model()
        cls.user0 = cls.User.objects.create_user('user0', 'user0@email.com', 'user0pass')


class SignupTest(SignupTestMixin, TestCase):
    """Verifica o processo de registro utilizando campos extras."""

    def test_signup_sucess(self):
        """Verifica se o registro é feito com sucesso."""
        self.assertEqual(self.User.objects.count(), 1)

        file_path = path.abspath(path.join(path.dirname(__file__), 'fixtures/favicon.ico'))
        with open(file_path, 'rb') as file:
            # mock image
            user_image = uploadedfile.UploadedFile(file, 'user1.jpg', 'img/jpeg')

            data = dict(
                first_name='Primeiro Usuário',
                email='user1@domain.com',
                username='user1',
                password1='clique123',
                password2='clique123',
                phone='554390019981',
                friend=self.User.objects.get(username='user0').pk,
                user_image=user_image,
            )
            response = self.client.post(self.register_url, data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(self.User.objects.count(), 2)

            user1 = self.User.objects.get(username='user1')
            self.assertEqual(user1.phone, '554390019981')
            self.assertEqual(user1.friend.username, 'user0')
            self.assertEqual(user1.user_image.url, '/media/clickmob/user_images/user1/user1.jpg')

    def test_signup_error_invalid_friend(self):
        """Verifica se o processo de registro é rejeitado para um usuário inválido."""
        self.assertEqual(self.User.objects.count(), 1)

        data = dict(
            first_name='first name',
            email='user1@domain.com',
            username='user1',
            password1='clique123',
            password2='clique123',
            friend=1000,
        )
        response = self.client.post(self.register_url, data)
        error = response.context[0]['form'].errors['friend']
        self.assertIn('Faça uma escolha válida. Sua escolha não é uma das disponíveis.', error)

    def test_signup_default_image(self):
        """Verifica se a imagem default é salva."""
        data = dict(
            first_name='Primeiro Usuário',
            email='user1@domain.com',
            username='user1',
            password1='clique123',
            password2='clique123',
            phone='554390019981',
            friend=self.User.objects.get(username='user0').pk,
        )
        response = self.client.post(self.register_url, data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.User.objects.count(), 2)

        user1 = self.User.objects.get(username='user1')
        self.assertEqual(user1.phone, '554390019981')
        self.assertEqual(user1.friend.username, 'user0')
        self.assertEqual(user1.user_image.url, '/media/clickmob/user_images/user1/default_avatar.jpeg')


class RestSignupTest(SignupTestMixin, APITestCase):
    """Verifica o processo de registro utilizando campos extras através da API."""

    def test_signup_sucess(self):
        """Verifica se o registro é feito com sucesso utilizando a API."""
        self.assertEqual(self.User.objects.count(), 1)

        file_path = path.abspath(path.join(path.dirname(__file__), 'fixtures/favicon.ico'))
        with open(file_path, 'rb') as file:
            # mock image
            user_image = uploadedfile.UploadedFile(file, 'user1.jpg', 'img/jpeg')

            data = dict(
                first_name='Primeiro Usuário',
                email='user1@domain.com',
                username='user1',
                password1='clique123',
                password2='clique123',
                phone='554390019981',
                friend=self.User.objects.get(username='user0').pk,
                user_image=user_image,
            )
            response = self.client.post(self.rest_register_url, data)
            self.assertTrue(is_success(response.status_code))

        self.assertEqual(self.User.objects.count(), 2)

        user1 = self.User.objects.get(username='user1')
        self.assertEqual(user1.phone, '554390019981')
        self.assertEqual(user1.friend.username, 'user0')
        self.assertEqual(user1.user_image.url, '/media/clickmob/user_images/user1/user1.jpg')

    def test_signup_error_invalid_friend(self):
        """Verifica se o processo de registro é rejeitado para um usuário inválido utilizando a API."""
        self.assertEqual(self.User.objects.count(), 1)

        data = dict(
            first_name='first name',
            email='user1@domain.com',
            username='user1',
            password1='clique123',
            password2='clique123',
            friend=1000,
        )
        response = self.client.post(self.rest_register_url, data)
        self.assertEqual('Pk inválido "1000" - objeto não existe.', response.data['friend'][0])

    def test_signup_api_default_image(self):
        """Verifica se a imagem default é salva via API."""
        data = dict(
            first_name='Primeiro Usuário',
            email='user1@domain.com',
            username='user1',
            password1='clique123',
            password2='clique123',
            phone='554390019981',
            friend=self.User.objects.get(username='user0').pk,
        )
        response = self.client.post(self.rest_register_url, data)
        self.assertTrue(is_success(response.status_code))
        self.assertEqual(self.User.objects.count(), 2)

        user1 = self.User.objects.get(username='user1')
        self.assertEqual(user1.phone, '554390019981')
        self.assertEqual(user1.friend.username, 'user0')
        self.assertEqual(user1.user_image.url, '/media/clickmob/user_images/user1/default_avatar.jpeg')
