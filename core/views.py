"""Views da aplicação core."""

from core.pagination import CorePaginator
from django.contrib import auth
from django.views import generic
from rest_framework import filters, generics, permissions

from . import serializers


class UserListView(generics.ListAPIView):
    """Obtém a lista de usuários."""

    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.IsAdminUser,)
    queryset = auth.get_user_model().objects.exclude(is_superuser=True)
    pagination_class = CorePaginator
    search_fields = ['username', 'email']


user_list = UserListView.as_view()


class UserDetailView(generics.RetrieveUpdateAPIView):
    """Obtém ou atualiza informações do usuário logado. Deve ser utilizada no lugar de /rest_auth/user/."""

    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = auth.get_user_model().objects.all()

    def get_object(self):
        """Retorna o usuário logado."""
        return self.request.user


user_detail = UserDetailView.as_view()


class FriendView(generics.ListAPIView):
    """Obtém uma lista de usuários para serem utilizados como amigos no processo de registro."""

    serializer_class = serializers.FriendSerializer
    pagination_class = CorePaginator
    queryset = auth.get_user_model().objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['username']

    def get_queryset(self):
        """Obtém o queryset. Nesse caso, admins não podem ser selecionados como amigo."""
        return super().get_queryset().exclude(is_superuser=True)


friends = FriendView.as_view()


class ProfileView(generics.CreateAPIView):
    """Cria o profile do usuário."""

    serializer_class = serializers.ProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        """Passa o usuário logado para o serializador."""
        serializer.save(user=self.request.user)


profile = ProfileView.as_view()


class UserInfoTemplateView(generic.TemplateView):
    """Obtém o template server side para o componente navbar."""

    template_name = 'core/user_info_template.html'


user_info_template = UserInfoTemplateView.as_view()


class SettingsTemplateView(generic.TemplateView):
    """Obtém o template server side para o componente navbar."""

    template_name = 'core/settings_template.html'


settings_template = SettingsTemplateView.as_view()
