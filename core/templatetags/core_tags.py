"""Este módulo contem tags e filtros globais em relação ao projeto."""

import hashlib

from django import template
from django.contrib.staticfiles.templatetags import staticfiles

register = template.Library()


@register.simple_tag
def gravatar_url(email):
    """Obtém a url gravatar em função do email fornecido."""
    return '//www.gravatar.com/avatar/{}'.format(hashlib.md5(email.encode('utf-8')).hexdigest())


@register.simple_tag(takes_context=True)
def user_image(context, image_path='img/default_avatar.jpeg'):
    """Obtém a imagem de perfil de um usuário, ou o default, definido pelo argumento image_path."""
    user = context['request'].user
    return user.user_image.url if user.user_image else staticfiles.static(image_path)
