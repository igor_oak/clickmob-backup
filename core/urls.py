"""Core urls."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^friends/$', views.friends, name='friends'),
    urls.url(r'^profile/$', views.profile, name='profile'),
    urls.url(r'^users/$', views.user_list, name='user_list'),
    urls.url(r'^users/detail/$', views.user_detail, name='user_detail'),
    urls.url(r'^navbar/template/$', views.user_info_template, name='user_info_template'),
    urls.url(r'^settings/template/$', views.settings_template, name='settings_template'),
]
