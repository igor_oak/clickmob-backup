"""Serializadores da aplicação core."""

from django.contrib import auth
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import exceptions, serializers

from . import models


class ProfileSerializer(serializers.ModelSerializer):
    """Serializador do perfil do usuário."""

    def create(self, validated_data):
        """Cria um profile para esse usuário."""
        user = validated_data.pop('user')
        return models.Profile.objects.update_or_create(user=user, defaults=validated_data)[0]

    class Meta:
        model = models.Profile
        fields = ['sacado_documento', 'sacado_uf', 'sacado_cidade', 'sacado_bairro', 'sacado_logradouro',
                  'sacado_numero', 'sacado_cep']


class UserSerializer(serializers.ModelSerializer):
    """User serializer."""

    total_items = serializers.ReadOnlyField(source='shoppingcart.shopping_cart_items.count')
    profile = ProfileSerializer(read_only=True)

    def update(self, instance, validated_data):
        """Atualiza um usuário."""
        # nesse caso uma imagem não foi enviada, portanto, a imagem que por ventura já exista não deve
        # ser sobrescrita.
        if 'user_image' in validated_data and not validated_data['user_image']:
            del validated_data['user_image']

        return super().update(instance, validated_data)

    class Meta:
        model = auth.get_user_model()
        fields = ['id', 'username', 'email', 'phone', 'country', 'user_image', 'gender',
                  'first_name', 'created', 'n_views', 'user_points', 'network_points', 'user_type',
                  'is_superuser', 'total_items', 'profile']

        read_only_fields = ['username', 'email', 'created', 'user_points', 'network_points', 'user_type',
                            'is_superuser', 'n_views']
        extra_kwargs = {
            'first_name': {
                'required': True,
                'allow_blank': False
            }
        }


class FriendSerializer(serializers.ModelSerializer):
    """Serializador de usuários com campos restritos."""

    class Meta:
        model = auth.get_user_model()
        fields = ['id', 'username', 'first_name']


class DisableSignupSerializer(serializers.Serializer):
    """Serializador customizado."""

    def validate(self, data):
        """Desativa a criação de novas contas."""
        raise exceptions.ValidationError('Registro de novas contas temporariamente suspenso.')


class CoreSignupSerializer(RegisterSerializer):
    """Serializador customizado para trabalhar com os campos extras do registro."""

    first_name = serializers.CharField(max_length=30)
    user_image = serializers.ImageField(required=False)
    phone = serializers.CharField(required=False, max_length=16)
    friend = serializers.PrimaryKeyRelatedField(queryset=auth.get_user_model().objects.all(),
                                                allow_null=True,
                                                required=False)

    def custom_signup(self, request, user):
        """no op."""

    def get_cleaned_data(self):
        """Mapa de objetos python."""
        cleaned_data = super().get_cleaned_data()
        cleaned_data['first_name'] = self.validated_data['first_name']
        cleaned_data['phone'] = self.validated_data.get('phone', '')
        cleaned_data['user_image'] = self.validated_data.get('user_image', None)
        cleaned_data['is_affiliated'] = self.validated_data.get('is_affiliated', False)
        if 'friend' not in self.validated_data:
            self.validated_data['friend'] = None

        cleaned_data['friend'] = self.validated_data['friend']
        return cleaned_data
