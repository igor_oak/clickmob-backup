"""Formulários da aplicação core."""

from django import forms
from django.contrib import auth


class CoreSignupForm(forms.Form):
    """Formulário de registro contendo campos extras."""

    first_name = forms.CharField(required=True, max_length=32)
    user_image = forms.ImageField(required=False)
    phone = forms.CharField(required=False, max_length=16)
    friend = forms.ModelChoiceField(
        queryset=auth.get_user_model().objects.all(),
        empty_label='-- ID do amigo --',
        required=False,
        widget=forms.widgets.TextInput
    )
    is_affiliated = forms.BooleanField(required=False)

    def signup(self, request, user):
        """no op."""
