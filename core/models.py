"""Modelos da aplicação core."""

import decimal
import shutil
from os import path

from django import dispatch
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import signals
from model_utils.models import TimeStampedModel

gender_choices = [('', ''), ('M', 'Masculino'), ('F', 'Feminino'), ('O', 'Outros')]


def user_image_folder(instance, filename):
    """Define o diretório para as imagens de usuário."""
    return 'clickmob/user_images/{0}/{1}'.format(instance.username, filename)


class User(TimeStampedModel, AbstractUser):
    """Usuário base do projeto."""

    user_image = models.ImageField(upload_to=user_image_folder, null=True, blank=True)
    phone = models.CharField(max_length=16, blank=True)
    country = models.CharField(max_length=15, default='Brasil')
    gender = models.CharField(max_length=2, choices=gender_choices, blank=True)

    friend = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='friends', blank=True)
    user_type = models.PositiveIntegerField(default=0)
    user_points = models.PositiveIntegerField(default=0)
    network_points = models.PositiveIntegerField(default=0)
    n_views = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Usuário'
        ordering = ('-created',)


class BankBalance(models.Model):
    """Saldo do usuário no mês."""

    status_choices = [('Parcial', 'Parcial'), ('Integrado', 'Integrado'), ('Enviado', 'Enviado'),
                      ('Pago', 'Pago')]

    status = models.CharField(max_length=10, default='parcial', choices=status_choices)
    balance = models.DecimalField(decimal_places=2, max_digits=7, default=decimal.Decimal('0.00'))
    month = models.DateTimeField()
    limit_value = models.DecimalField(decimal_places=2, max_digits=7)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)


class Profile(models.Model):
    """A criação deste profile é necessária para permissão da compra de publicidade."""

    user = models.OneToOneField(User)
    sacado_uf = models.CharField(max_length=2)
    sacado_cidade = models.CharField(max_length=40)
    sacado_bairro = models.CharField(max_length=40)
    sacado_cep = models.CharField(max_length=10)
    sacado_numero = models.CharField(max_length=10)
    sacado_logradouro = models.CharField(max_length=255)
    sacado_documento = models.CharField(max_length=40)


@dispatch.receiver(signals.post_delete, sender=User)
def auto_delete_files_on_delete(sender, instance, **kwargs):
    """
    Remove o diretório contendo arquivos do usuário na deleção do mesmo.

    Atenção extrema em relação ao diretório a ser removido para evitar possíveis desastres.
    """
    if instance.user_image:
        shutil.rmtree(path.dirname(instance.user_image.path), ignore_errors=True)
