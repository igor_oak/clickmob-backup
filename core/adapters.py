"""Adaptadores da aplicação core."""

from allauth.account import adapter
from allauth.account.utils import user_email, user_field, user_username
from core import utils
from django.db.models.fields import files


class DisableSignupAdapter(adapter.DefaultAccountAdapter):
    """Adaptador customizado."""

    def is_open_for_signup(self, request):
        """Desativa o processo de registro."""
        return False


class CoreAccountAdapter(adapter.DefaultAccountAdapter):
    """Adaptador customizado."""

    def save_user(self, request, user, form, commit=True):
        """Salva os campos adicionais do usuário."""
        data = form.cleaned_data
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        email = data.get('email')
        username = data.get('username')
        user_email(user, email)
        user_username(user, username)

        if first_name:
            user_field(user, 'first_name', first_name)

        if last_name:
            user_field(user, 'last_name', last_name)

        if 'password1' in data:
            user.set_password(data["password1"])

        else:
            user.set_unusable_password()

        self.populate_username(request, user)

        user.phone = data.get('phone', '')
        user.friend = data.get('friend', None)
        user.user_image = data.get('user_image', None)
        if data.get('is_affiliated', False):
            user.user_type = 1

        if user.user_image.name is None:
            fname = '../clickmob/static/img/default_avatar.jpeg'
            fname = utils.get_path_from_dir(fname, __file__)
            with open(fname, 'rb') as f:
                user.user_image.save('default_avatar.jpeg', files.File(f), commit)
                return user

        if commit:
            user.save()

        return user
