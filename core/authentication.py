"""Autenticação extra da aplicação core."""

from rest_framework import authentication


class QueryStringTokenAuthentication(authentication.TokenAuthentication):
    """Autenticador que utiliza o token enviado via query string."""

    param_name = 'auth_token'

    def authenticate(self, request):
        """Efetua a autenticação."""
        if self.param_name not in request.query_params:
            return None

        return self.authenticate_credentials(request.query_params.get(self.param_name, ''))
