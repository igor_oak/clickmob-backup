"""Core permissions."""

from rest_framework import permissions


class CoreModelPermissions(permissions.DjangoModelPermissions):
    """Permissões genéricas para modelos."""

    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


class IsUserOrAdmin(permissions.BasePermission):
    """Acesso apenas ao usuário ou o Admin."""

    def has_object_permission(self, request, view, obj):
        """Verifica a permissão ao objeto."""
        return request.user.is_superuser or request.user == obj

    def has_permission(self, request, view):
        """Verifica a permissão."""
        return request.user.is_superuser or request.user.is_authenticated()


class IsOwnerOrAdmin(permissions.BasePermission):
    """Acesso apenas ao dono do recurso ou o Admin."""

    def has_object_permission(self, request, view, obj):
        """Verifica a permissão ao objeto."""
        return request.user.is_superuser or request.user == obj.owner

    def has_permission(self, request, view):
        """Verifica a permissão."""
        return request.user.is_superuser or request.user.is_authenticated()
