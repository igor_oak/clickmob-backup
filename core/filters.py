"""Filtros da aplicação core."""

import django_filters
from django.contrib import auth


class UserFilter(django_filters.FilterSet):
    """Filtro de usuários."""

    username = django_filters.CharFilter(name='username', lookup_expr='icontains')

    class Meta:
        model = auth.get_user_model()
        fields = ['username']
