"""URLs da aplicação orders."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^$', views.orders, name='orders'),
    urls.url(r'^shopping/cart/$', views.shopping_cart, name='shopping_cart'),
    urls.url(r'^shopping/cart/(?P<pk>\d+)/$', views.shopping_cart_item, name='shopping_cart_item'),
    urls.url(r'^shopping/cart/template/$', views.shopping_cart_template, name='shopping_cart_template'),
    urls.url(r'^order/history/template/$', views.order_history_template, name='order_history_template'),
]
