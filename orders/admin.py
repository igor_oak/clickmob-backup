"""Administração da aplicação orders."""

from django.contrib import admin

from . import models


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    """Order na interface admin."""
