"""Views da aplicação orders."""

from core.pagination import CorePaginator
from core.permissions import IsOwnerOrAdmin
from django.views import generic
from rest_framework import generics, response, status

from . import models, permissions, serializers


class OrdersView(generics.ListCreateAPIView):
    """API para pedidos."""

    serializer_class = serializers.OrderSerializer
    permission_classes = (IsOwnerOrAdmin, permissions.ActiveProfile)
    queryset = models.Order.objects.all()
    pagination_class = CorePaginator

    def get_queryset(self):
        """Retorna pedidos do usuário."""
        return super().get_queryset().filter(owner=self.request.user)

    def perform_create(self, serializer):
        """Passa o usuário logado ao serializador."""
        return serializer.save(owner=self.request.user)

    def create(self, request, *args, **kwargs):
        """Usa implementação default mas retorna o carrinho de compras para atualização."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        data = self.get_serializer(order).data
        return response.Response(data, status=status.HTTP_201_CREATED, headers=headers)


orders = OrdersView.as_view()


class ShoppingCartView(generics.RetrieveUpdateDestroyAPIView):
    """API para carrinho de compras."""

    serializer_class = serializers.ShoppingCartSerializer
    permission_classes = (IsOwnerOrAdmin, permissions.ActiveProfile)
    queryset = models.ShoppingCart.objects.all()
    pagination_class = None

    def get_object(self):
        """Obtém o carrinho de compras do usuário."""
        return models.ShoppingCart.objects.get_or_create(owner=self.request.user)[0]

    def perform_create(self, serializer):
        """Passa o usuário logado ao serializador."""
        serializer.save(owner=self.request.user)

    def destroy(self, request, *args, **kwargs):
        """Limpa o carrinho."""
        instance = self.get_object()
        self.perform_destroy(instance)
        data = self.get_serializer(instance=instance).data
        return response.Response(data)


shopping_cart = ShoppingCartView.as_view()


class ShoppingCartItemView(generics.DestroyAPIView):
    """API para items do carrinho de compras."""

    serializer_class = serializers.ShoppingCartItemSerializer
    permission_classes = (permissions.IsCartItemOwnerOrAdmin, permissions.ActiveProfile)
    queryset = models.ShoppingCartItem.objects.all()
    pagination_class = None

    def destroy(self, request, *args, **kwargs):
        """Limpa o carrinho."""
        instance = self.get_object()
        self.perform_destroy(instance)
        instance.shopping_cart.update_totals()
        data = serializers.ShoppingCartSerializer(instance.shopping_cart).data
        return response.Response(data)


shopping_cart_item = ShoppingCartItemView.as_view()


class ShoppingCartTemplate(generic.TemplateView):
    """Server side partial para o carrinho de compras."""

    template_name = 'orders/shopping_cart_template.html'


shopping_cart_template = ShoppingCartTemplate.as_view()


class OrderHistoryTemplate(generic.TemplateView):
    """Server side partial para histórico de compras."""

    template_name = 'orders/order_history_template.html'


order_history_template = OrderHistoryTemplate.as_view()
