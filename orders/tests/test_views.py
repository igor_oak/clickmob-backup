"""Testa as views da aplicação orders."""

import decimal
from unittest import mock

from core.models import Profile
from django.contrib import auth
from django.core import urlresolvers
from packs.models import Pack
from rest_framework import status, test

from .. import models


class FakeResponse:
    """Fake response para API externas."""

    status_code = 200

    def json(self):
        """no op."""
        return dict(boleto='hash')


class CartViewTest(test.APITestCase):
    """Testa a API para pedidos e carrinhos de compra."""

    @classmethod
    def setUpTestData(cls):
        """Class fixtures."""
        super().setUpTestData()
        cls.User = auth.get_user_model()
        cls.user1 = cls.User.objects.create_user('user1', 'user1@domain.com', 'pass123', user_type=1)
        Profile.objects.create(
            sacado_uf='PA', sacado_bairro='bairro', sacado_cidade='city',
            sacado_cep='12456789', sacado_numero='351', sacado_documento='12345678912',
            sacado_logradouro='endereço', user=cls.user1
        )
        cls.pack1 = Pack.objects.create(name='Pack1', price=decimal.Decimal('12.35'), n_views=500)
        cls.pack2 = Pack.objects.create(name='Pack2', price=decimal.Decimal('22.47'), n_views=1700)
        cls.orders_url = urlresolvers.reverse('orders:orders')
        cls.shopping_cart_url = urlresolvers.reverse('orders:shopping_cart')

    def test_retrieve_cart(self):
        """Verifica a API para carrinhos de compra."""
        self.client.login(username='user1', password='pass123')
        response = self.client.get(self.shopping_cart_url)
        cart = response.data
        self.assertListEqual(cart['shopping_cart_items'], [])

    def test_add_pack_to_cart(self):
        """Verifica se items estão sendo adicionados corretamente ao carrinho de compras."""
        self.client.login(username='user1', password='pass123')

        # adiciona 3x o primeiro pack.
        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        # adiciona 1x o segundo pack
        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack2.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        # nesse caso é esperado apenas 2 items no carrinho, primeiro e segundo pack, mas com sua quantidade do
        # primeiro pack triplicada.
        response = self.client.get(self.shopping_cart_url)
        shopping_cart = response.data
        self.assertTrue(len(shopping_cart['shopping_cart_items']), 2)
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['pack']['name'], 'Pack2')
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['quantity'], 1)
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['total_price'], '22.47')
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['total_n_views'], 1700)

        self.assertEqual(shopping_cart['shopping_cart_items'][1]['pack']['name'], 'Pack1')
        self.assertEqual(shopping_cart['shopping_cart_items'][1]['quantity'], 3)
        self.assertEqual(shopping_cart['shopping_cart_items'][1]['total_price'], '37.05')
        self.assertEqual(shopping_cart['shopping_cart_items'][1]['total_n_views'], 1500)

        self.assertEqual(shopping_cart['total_price'], '59.52')
        self.assertEqual(shopping_cart['total_n_views'], 3200)

    def test_delete_cart_item(self):
        """Verifica se um item do carrinho é removido e os totais são atualizados com sucesso."""
        self.client.login(username='user1', password='pass123')
        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        response = self.client.put(self.shopping_cart_url, data, format='json')
        item_pk = response.data['shopping_cart_items'][0]['id']

        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack2.id,
                    quantity=1
                )
            ]
        )
        self.client.put(self.shopping_cart_url, data, format='json')

        # um item é removido
        self.client.delete(urlresolvers.reverse('orders:shopping_cart_item', args=[item_pk]))

        # os valores do carrinho são atualizados.
        response = self.client.get(self.shopping_cart_url)
        shopping_cart = response.data
        self.assertTrue(len(shopping_cart['shopping_cart_items']), 1)
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['pack']['name'], 'Pack2')
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['quantity'], 1)
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['total_price'], '22.47')
        self.assertEqual(shopping_cart['shopping_cart_items'][0]['total_n_views'], 1700)

    def test_create_order(self):
        """Verifica se um pedido é criado corretamente a partir de um carrinho de compras do usuário."""
        with mock.patch('requests.post', return_value=FakeResponse()):
            self.assertEqual(models.Order.objects.count(), 0)
            self.client.login(username='user1', password='pass123')
            data = dict(
                shopping_cart_items=[
                    dict(
                        pack=self.pack1.id,
                        quantity=1
                    )
                ]
            )
            self.client.put(self.shopping_cart_url, data, format='json')

            data = dict(
                shopping_cart_items=[
                    dict(
                        pack=self.pack1.id,
                        quantity=1
                    )
                ]
            )
            self.client.put(self.shopping_cart_url, data, format='json')

            data = dict(
                shopping_cart_items=[
                    dict(
                        pack=self.pack2.id,
                        quantity=1
                    )
                ]
            )
            self.client.put(self.shopping_cart_url, data, format='json')

            response = self.client.post(self.orders_url)
            self.assertTrue(status.is_success(response.status_code))
            self.assertEqual(models.Order.objects.count(), 1)

            order = models.Order.objects.get()
            self.assertEqual(order.total_price, decimal.Decimal('47.17'))
            self.assertEqual(order.total_n_views, 2700)

        self.assertEqual(self.User.objects.get(username='user1').shoppingcart.shopping_cart_items.count(), 0)

    def test_create_order_empty_cart(self):
        """Verifica se o pedido não é criado para um carrinho vazio."""
        self.client.login(username='user1', password='pass123')
        response = self.client.post(self.orders_url)
        self.assertTrue(status.is_client_error(response.status_code))
        self.assertEqual(response.data['shopping_cart'], 'Carrinho vazio')

        # adiciona e remove um item.
        data = dict(
            shopping_cart_items=[
                dict(
                    pack=self.pack1.id,
                    quantity=1
                )
            ]
        )
        response = self.client.put(self.shopping_cart_url, data, format='json')
        item_pk = response.data['shopping_cart_items'][0]['id']
        self.client.delete(urlresolvers.reverse('orders:shopping_cart_item', args=[item_pk]))

        response = self.client.post(self.orders_url)
        self.assertTrue(status.is_client_error(response.status_code))
        self.assertEqual(response.data['shopping_cart'], 'Carrinho vazio')
