"""Permissões da aplicação orders."""

from rest_framework import permissions
from core.models import Profile


class IsCartItemOwnerOrAdmin(permissions.BasePermission):
    """Verifica se o usuário é dono do carrinho que contém esse item."""

    def has_object_permission(self, request, view, obj):
        """Verifica a permissão ao objeto."""
        return request.user.is_superuser or request.user == obj.shopping_cart.owner

    def has_permission(self, request, view):
        """Verifica a permissão."""
        return request.user.is_superuser or request.user.is_authenticated() and request.user.user_type >= 1


class ActiveProfile(permissions.BasePermission):
    """Verifica se o usuário tem permissão para compra de packs."""

    def has_permission(self, request, view):
        """Verifica se o usuário tem profile ativo."""
        if request.user.is_superuser:
            return True

        try:
            request.user.profile
            return True

        except Profile.DoesNotExist:
            return False
