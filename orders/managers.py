"""Managers da aplicação orders."""

import datetime

from django.db.models import Manager
from mibank.models import Boleto

from . import models


class ShoppingCartItemManager(Manager):
    """Gerenciador para items do carrinho de compra."""

    def update_shopping_cart_items(self, shopping_cart, **kwargs):
        """Atualiza items de um carrinho de compras."""
        try:
            # se o item já existe, atualize sua quantidade em função do valor enviado pelo cliente.
            obj = self.model.objects.get(
                shopping_cart=shopping_cart,
                pack__id=kwargs['pack'].id
            )
            obj.quantity += kwargs['quantity']  # quantidade pode ser negativa.
            if obj.quantity <= 0:  # nesse caso a quantidade to item chegou a 0, remover.
                obj.delete()

            else:
                obj.save()

        # se o item não existe no carrinho, adicione um novo.
        except self.model.DoesNotExist:
            self.model.objects.create(shopping_cart=shopping_cart, **kwargs)


class OrderManager(Manager):
    """Gerenciador para pedidos."""

    def create_order(self, user):
        """Cria um pedido a partir do carrinho de compras do usuário."""
        shopping_cart = user.shoppingcart

        order = self.model.objects.create(
            owner=user,
            total_price=shopping_cart.total_price,
            total_n_views=shopping_cart.total_n_views,
        )

        for shopping_cart_item in shopping_cart.shopping_cart_items.all():
            models.OrderCartItem.objects.create(
                order=order,
                pack=shopping_cart_item.pack,
                quantity=shopping_cart_item.quantity,
                total_price=shopping_cart_item.total_price,
                total_n_views=shopping_cart_item.total_n_views,
            )

        shopping_cart.delete()

        profile = user.profile
        vencimento = datetime.date.today() + datetime.timedelta(days=5)
        data = {
            'valor': order.total_price,
            'sacado_uf': profile.sacado_uf,
            'sacado_cep': profile.sacado_cep,
            'vencimento': vencimento,
            'sacado_nome': user.first_name,
            'sacado_email': user.email,
            'sacado_bairro': profile.sacado_bairro,
            'sacado_cidade': profile.sacado_cidade,
            'sacado_numero': profile.sacado_numero,
            'numero_controle': '{:04d}'.format(order.id),
            'sacado_documento': profile.sacado_documento,
            'sacado_logradouro': profile.sacado_logradouro,
        }
        boleto = Boleto.objects.gerar_boleto(data)
        order.boleto = boleto
        order.save()
        return order
