"""Modelos da aplicação carts."""

from django.conf import settings
from django.core import urlresolvers
from django.db import models
from mibank.models import Boleto
from packs.models import Pack

from . import managers


class CartItemFields(models.Model):
    """Campos comuns dos items de carrinho."""

    quantity = models.PositiveIntegerField()
    total_price = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    total_n_views = models.PositiveIntegerField(default=0)

    def save(self, *args, **kwargs):
        """Atualiza valores do item a partir de valores do pack."""
        self.total_n_views = self.pack.n_views * self.quantity
        self.total_price = self.pack.price * self.quantity
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True


class CartOrderFields(models.Model):
    """Campos comuns para o carrinho e pedido."""

    total_price = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    total_n_views = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True


class ShoppingCart(CartOrderFields):
    """Carrinho de compras."""

    owner = models.OneToOneField(settings.AUTH_USER_MODEL)

    def update_totals(self, *args, **kwargs):
        """Atualiza valores função dos items do carrinho."""
        self.total_price = 0
        self.total_n_views = 0
        for shopping_cart_item in self.shopping_cart_items.all():
            self.total_price += shopping_cart_item.total_price
            self.total_n_views += shopping_cart_item.total_n_views

        return self.save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """Limpa o carrinho de compras."""
        self.shopping_cart_items.all().delete()
        self.update_totals()
        return self


class ShoppingCartItem(CartItemFields):
    """Um item do carrinho de compras do usuário."""

    shopping_cart = models.ForeignKey(ShoppingCart, related_name='shopping_cart_items')
    pack = models.ForeignKey(Pack, related_name='shopping_cart_items')

    objects = managers.ShoppingCartItemManager()

    def get_absolute_url(self):
        """Obtém a URL para esse item do carrinho."""
        return urlresolvers.reverse('orders:shopping_cart_item', args=[self.pk])

    class Meta:
        ordering = ['-id']


class Order(CartOrderFields):
    """Pedido."""

    order_choices = [('A Pagar', 'A Pagar'), ('Pago', 'Pago')]

    buy_date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='orders')
    status = models.CharField(max_length=15, default='A Pagar', choices=order_choices)
    boleto = models.OneToOneField(Boleto, null=True)

    objects = managers.OrderManager()

    def confirmar_pagamento(self):
        """Confirma o pagamento do pedido, atribuindo o total de views ao montante do usuário."""
        self.status = 'Pago'
        self.owner.n_views += self.total_n_views
        self.owner.save()
        self.save()

    def __str__(self):
        """toString."""
        return '[{}][{}]'.format(self.owner.username, self.buy_date)

    class Meta:
        ordering = ['-buy_date']


class OrderCartItem(CartItemFields):
    """Um item do pedido."""

    order = models.ForeignKey(Order, related_name='order_cart_items')
    pack = models.ForeignKey(Pack, related_name='order_cart_items')
