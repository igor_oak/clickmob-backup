"""Serializadores da aplicação orders."""

from packs.serializers import PackSerializer
from rest_framework import exceptions, serializers

from . import models


class OrderCartItemSerializer(serializers.ModelSerializer):
    """Serializador para items do pedido."""

    quantity = serializers.IntegerField()

    def to_representation(self, cart_item):
        """to repr."""
        data = super().to_representation(cart_item)
        data['pack'] = PackSerializer(instance=cart_item.pack).data
        return data

    class Meta:
        model = models.OrderCartItem
        fields = ['id', 'pack', 'quantity', 'total_price', 'total_n_views']
        read_only_fields = ['total_price', 'total_n_views']


class OrderSerializer(serializers.ModelSerializer):
    """Serializador de pedidos."""

    order_cart_items = OrderCartItemSerializer(read_only=True, many=True)
    boleto_hash = serializers.ReadOnlyField(source='boleto.boleto')

    def save(self, **kwargs):
        """Cria um pedido."""
        user = kwargs['owner']
        try:
            if user.shoppingcart.shopping_cart_items.count() == 0:
                raise models.ShoppingCart.DoesNotExist

        except models.ShoppingCart.DoesNotExist:
            raise exceptions.ValidationError({'shopping_cart': 'Carrinho vazio'})

        return models.Order.objects.create_order(user)

    class Meta:
        model = models.Order
        fields = ['id', 'order_cart_items', 'buy_date', 'status', 'total_price', 'total_n_views',
                  'boleto_hash']
        read_only_fields = ['buy_date', 'status', 'total_price', 'total_n_views']


class ShoppingCartItemSerializer(OrderCartItemSerializer):
    """Serializador para items do carrinho."""

    absolute_url = serializers.ReadOnlyField(source='get_absolute_url')

    class Meta(OrderCartItemSerializer.Meta):
        model = models.ShoppingCartItem
        fields = OrderCartItemSerializer.Meta.fields + ['absolute_url']
        read_only_fields = OrderCartItemSerializer.Meta.read_only_fields


class ShoppingCartSerializer(serializers.ModelSerializer):
    """Serializador do carrinho de compras."""

    shopping_cart_items = ShoppingCartItemSerializer(many=True)

    def update(self, instance, validated_data):
        """Atualiza o carrinho de compras."""
        shopping_cart_items = validated_data.pop('shopping_cart_items', [])
        for shopping_cart_item in shopping_cart_items:
            models.ShoppingCartItem.objects.update_shopping_cart_items(instance, **shopping_cart_item)

        instance.update_totals()
        return instance

    class Meta:
        model = models.ShoppingCart
        fields = ['id', 'shopping_cart_items', 'total_price', 'total_n_views']
        read_only_fields = ['total_price', 'total_n_views']
