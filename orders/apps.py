"""Configuração da aplicação orders."""

from django.apps import AppConfig


class OrdersConfig(AppConfig):
    """Configura a aplicação."""

    name = 'orders'
