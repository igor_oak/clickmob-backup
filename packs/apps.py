"""Configuração da aplicação packs."""

from django.apps import AppConfig


class PacksConfig(AppConfig):
    """Configura a aplicação."""

    name = 'packs'
