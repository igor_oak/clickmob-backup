"""Views da aplicação packs."""

from core import pagination
from django.views import generic
from rest_framework import generics

from . import models, permissions, serializers


class PackListView(generics.ListCreateAPIView):
    """
    Obtém a lista de packs para usuários logados.

    Esta lista de packs é utilizada na página "comprar packs".
    """

    serializer_class = serializers.PackSerializer
    permission_classes = (permissions.PackPermission,)
    queryset = models.Pack.objects.all()
    pagination_class = pagination.CorePaginator
    search_fields = ['name']


packs_list = PackListView.as_view()


class BuyPacksTemplate(generic.TemplateView):
    """Server side buy pack partial."""

    template_name = 'packs/buy_packs_template.html'


buy_packs_template = BuyPacksTemplate.as_view()


class CreatePacksTemplate(generic.TemplateView):
    """Server side create packs partial."""

    template_name = 'packs/create_packs_template.html'


create_packs_template = CreatePacksTemplate.as_view()
