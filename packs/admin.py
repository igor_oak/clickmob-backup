"""Administração da aplicação packs."""

from django.contrib import admin

from . import models


@admin.register(models.Pack)
class PackAdmin(admin.ModelAdmin):
    """Gerencia packs na interface administrativa."""
