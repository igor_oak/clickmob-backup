"""URLs da aplicação packs."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^list/$', views.packs_list, name='packs_list'),
    urls.url(r'^buy/packs/template/$', views.buy_packs_template, name='buy_packs_template'),
    urls.url(r'^create/packs/template/$', views.create_packs_template, name='create_packs_template'),
]
