"""Serializadores da aplicação packs."""

from rest_framework import serializers

from . import models


class PackSerializer(serializers.ModelSerializer):
    """Serializador de packs."""

    class Meta:
        model = models.Pack
        fields = ['id', 'name', 'price', 'n_views']
