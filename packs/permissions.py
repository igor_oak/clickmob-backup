"""Permissões extras da aplicação packs."""

from rest_framework import permissions


class PackPermission(permissions.BasePermission):
    """Permissão de listagem para o usuário logado e criação para o admin."""

    def has_object_permission(self, request, view, obj):
        """Admin tem permissão sobre o objeto."""
        return request.user.is_superuser

    def has_permission(self, request, view):
        """Usuário pode apenas listar objetos."""
        if request.user.is_superuser:
            return True

        if (request.user.is_authenticated() and
            request.method in permissions.SAFE_METHODS and
                request.user.user_type >= 1):
            return True

        return False
