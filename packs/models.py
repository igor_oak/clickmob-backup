"""Modelos da aplicação packs."""

from django.db import models
from model_utils.models import TimeStampedModel


class Pack(TimeStampedModel, models.Model):
    """Um pack contendo visualizações que podem ser utilizadas pelo usuário para criação de campanhas."""

    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    n_views = models.PositiveIntegerField()

    def __str__(self):
        """toString."""
        return '[Nome={}][Preço={}][Visualizações={}]'.format(self.name, self.price, self.n_views)

    class Meta:
        ordering = ['-created']
