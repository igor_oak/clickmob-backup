"""URLs da aplicação campaigns."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^show/$', views.show_campaign, name='show_campaign'),
    urls.url(r'^$', views.list_campaigns, name='list_campaigns'),
    urls.url(r'^(?P<pk>\d+)/$', views.campaign_detail, name='campaign_detail'),
    urls.url(r'^(?P<pk>\d+)/activate/$', views.activate_campaign, name='activate_campaign'),
    urls.url(r'^(?P<pk>\d+)/cancel/$', views.cancel_campaign, name='cancel_campaign'),
    urls.url(r'^(?P<pk>\d+)/viewers/log/$', views.campaign_viewers, name='campaign_viewers'),
    urls.url(r'^(?P<pk>\d+)/chart/data/$', views.campaign_viewers_chart, name='campaign_viewers_chart'),
    # urls.url(r'^view/campaign/$', views.view_campaign, name='view_campaign'),
    urls.url(r'^view/campaign/redirect/$', views.view_campaign_redirect, name='view_campaign_redirect'),
    urls.url(r'^list/campaigns/template/$', views.list_campaigns_template, name='list_campaigns_template'),
    urls.url(r'^new/campaign/template/$', views.new_campaign_template, name='new_campaign_template'),
    urls.url(r'^campaign/detail/template/$', views.campaign_detail_template, name='campaign_detail_template'),
]
