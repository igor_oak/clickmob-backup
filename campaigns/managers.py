"""Managers da aplicação clickmob."""

from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.db.models import F, Manager
from django.utils import timezone


class GeoCampaignManager(models.GeoManager):
    """Campaign manager com suporte para geolocalização."""

    def _build_params(self, **kwargs):
        """Constroe os parâmetros de busca para o filtro do queryset."""
        date = timezone.now()
        age = kwargs.get('age', [0])[0]
        gender = kwargs.get('gender', [''])[0]

        params = dict(is_active=True, n_views__gte=1, age__lte=age, start_date__lte=date, end_date__gte=date,
                      gender=gender, is_canceled=False)
        return params

    def nearby_campaigns(self, latitude, longitude, **kwargs):
        """Encontra campanhas proximas da latitude e longitude."""
        params = self._build_params(**kwargs)
        params['distance__lte'] = F('radius')

        user_location = Point(float(longitude), float(latitude), srid=4326)
        return self.model.objects.annotate(distance=Distance('center', user_location)).filter(**params)

    def by_city(self, city, **kwargs):
        """Encontra uma campanha em função da cidade informada."""
        params = self._build_params(**kwargs)
        params['cities__contains'] = city
        return self.model.objects.filter(**params)

    def by_state(self, state, **kwargs):
        """Encontra uma campanha em função do estado informado."""
        params = self._build_params(**kwargs)
        params['states__contains'] = state
        return self.model.objects.filter(**params)

    def by_country(self, country, **kwargs):
        """Encontra uma campanha em função do país informado."""
        params = self._build_params(**kwargs)
        params['countries__contains'] = country
        return self.model.objects.filter(**params)

    def general_campaigns(self, **kwargs):
        """Conjunto de campanhas que não se enquadram em nenhum outro algoritmo de busca."""
        params = self._build_params(**kwargs)
        return self.model.objects.filter(**params)


class UserCampaignViewManager(Manager):
    """Manager para o registro de visualizações de usuários."""

    def view_campaign(self, user, campaign, view_date=None):
        """Loga o registro de visualização da campanha e atribui pontos ao usuário."""
        kwargs = dict(user=user, campaign=campaign, interaction_type='Visualização')
        if view_date is not None:
            kwargs['view_date'] = view_date

        self.model.objects.create(**kwargs)

        campaign.n_views -= 1
        if campaign.n_views == 0:
            campaign.is_active = False
            campaign.status = 'Finalizada'

        campaign.save()

        user.user_points += 1
        user.save()

    def view_campaign_redirect(self, user, campaign, view_date=None):
        """Loga o registro de visualização da campanha e atribui pontos ao usuário."""
        kwargs = dict(user=user, campaign=campaign, interaction_type='Interação')
        if view_date is not None:
            kwargs['view_date'] = view_date

        self.model.objects.create(**kwargs)

        campaign.n_views -= 1
        if campaign.n_views == 0:
            campaign.is_active = False
            campaign.status = 'Finalizada'

        campaign.save()

        user.user_points += 10
        user.save()
