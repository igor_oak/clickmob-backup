"""Cria visualizações aleatórias para uma campanha."""

import datetime
import os
import random

from django.contrib import auth
from django.core.management import base

from ... import models


class Command(base.BaseCommand):
    """Comando."""

    def add_arguments(self, parser):
        """Configura argumentos da linha de comando."""
        parser.add_argument('username')
        parser.add_argument('campaign_id', type=int)
        parser.add_argument('--total', default=50, type=int)

    def handle(self, *args, **options):
        """Executa o comando."""
        if 'development' in os.environ['DJANGO_SETTINGS_MODULE']:
            user = auth.get_user_model().objects.get(username=options['username'])
            campaign = models.Campaign.objects.get(pk=options['campaign_id'])

            start_date = campaign.start_date
            end_date = campaign.end_date

            for i in range(options['total']):
                seconds = (end_date - start_date).total_seconds()
                seconds = random.randint(0, int(seconds))

                view_date = start_date + datetime.timedelta(seconds=seconds)
                if view_date.month % 2 == 0:
                    models.UserCampaignViewLog.objects.view_campaign(user, campaign, view_date=view_date)

                else:
                    models.UserCampaignViewLog.objects.view_campaign_redirect(
                        user, campaign, view_date=view_date
                    )

            print('Geradas {} visualizações para o usuário "{}" na campanha "{}"'.format(
                options['total'],
                options['username'],
                campaign.title
            ))
