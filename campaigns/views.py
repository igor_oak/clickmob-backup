"""Views da aplicação campaigns."""

import logging

from core.pagination import CorePaginator
from django import http, shortcuts
from django.db.models import Count, functions
from django.views import generic
from rest_framework import generics, response, status
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from . import models, permissions, serializers

logger = logging.getLogger(__name__)


class ListCampaignsView(generics.ListCreateAPIView):
    """
    Cria ou lista campanhas deste usuário. Requer token.

    (GET) É possível controlar a paginação utilizando page (qual página visualizar), page_size
    (total por página) e ordering (campo para ordenação). O parâmetro search é utilizado para filtrar
    resultados pelo título da campanha.
    """

    serializer_class = serializers.CampaignSerializer
    permission_classes = (permissions.IsCampaignOwnerOrAdmin,)
    queryset = models.Campaign.objects.all()
    pagination_class = CorePaginator
    search_fields = ['owner__username', 'title']

    def get_queryset(self):
        """Obtém as campanhas do usuário, ou todas, no caso do admin."""
        queryset = super().get_queryset()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(owner=self.request.user)

        return queryset

    def perform_create(self, serializer):
        """Atribui o usuário atual como dono do recurso."""
        serializer.save(owner=self.request.user)


list_campaigns = ListCampaignsView.as_view()


class CampaignDetailView(generics.RetrieveAPIView):
    """Obtém uma campanha por id."""

    serializer_class = serializers.CampaignDetailSerializer
    permission_classes = (permissions.IsCampaignOwnerOrAdmin,)
    queryset = models.Campaign.objects.all()


campaign_detail = CampaignDetailView.as_view()


class ActivateCampaignView(generics.UpdateAPIView):
    """Permite ao administrador atualizar a campanha."""

    serializer_class = serializers.AdminActivateCampaignSerializer
    permission_classes = (IsAdminUser,)
    queryset = models.Campaign.objects.all()


activate_campaign = ActivateCampaignView.as_view()


class CancelCampaignView(generics.UpdateAPIView):
    """Cancela uma campanha."""

    permission_classes = (permissions.IsCampaignOwnerOrAdmin,)
    serializer_class = serializers.CancelCampaignSerializer
    queryset = models.Campaign.objects.all()


cancel_campaign = CancelCampaignView.as_view()


class ShowCampaignView(generics.RetrieveAPIView):
    """
    Obtém uma campanha (propaganda) a partir de um algoritmo de seleção. Requer token.

    Esta API é utilizada para mostrar uma propaganda quando o usuário desbloqueia sua tela no celular.

    Esta API é utilizada quando o usuário desloqueia sua tela de celular utilizando o "cadeado". Nesse caso,
    ela diminui o número de visualizações da campanha em 1 e atribui +1 pontos para o usuário. Também registra
    qual usuário visualizou determinada campanha junto com o horário.
    """

    serializer_class = serializers.CampaignSerializer
    permission_classes = (IsAuthenticated,)
    queryset = models.Campaign.objects.all()
    pagination_class = None
    filter_backends = None

    def get_object(self):
        """Obtém uma campanha."""
        params = self.request.query_params.copy()
        try:
            # prioridade 5: raio informado. Nesse caso o usuário envia sua posição (lat, lng) e o sistema
            # tenta encontrar uma propaganda cuja abrangência (área formada pelo centro e raio da campanha)
            # inclua as coordenadas do usuário.
            latitude = params.pop('lat', [None])[0]
            longitude = params.pop('lng', [None])[0]
            if latitude is not None and longitude is not None:
                queryset = models.Campaign.objects.nearby_campaigns(latitude, longitude, **params)
                if queryset.exists():
                    # obtém uma campanha aleatória que abrange as coordenadas do usuário.
                    obj = queryset.order_by('?')[0]
                    self.check_object_permissions(self.request, obj)
                    logger.debug('Campanha obtida pela prioridade 5 - raio %s', obj.distance)
                    return obj

            # prioridade 4: cidade informada.
            city = params.pop('city', None)
            if city is not None:
                queryset = models.Campaign.objects.by_city(city, **params)
                if queryset.exists():
                    # obtém uma campanha aleatória que contém a cidade informada.
                    obj = queryset.order_by('?')[0]
                    self.check_object_permissions(self.request, obj)
                    logger.debug('Campanha obtida pela prioridade 4 - cidade')
                    return obj

            # prioridade 3: stado informado.
            state = params.pop('state', None)
            if state is not None:
                queryset = models.Campaign.objects.by_state(state, **params)
                if queryset.exists():
                    # obtém uma campanha aleatória que contém o estado informado.
                    obj = queryset.order_by('?')[0]
                    self.check_object_permissions(self.request, obj)
                    logger.debug('Campanha obtida pela prioridade 3 - estado')
                    return obj

            # prioridade 2: País informado.
            country = params.pop('country', None)
            if country is not None:
                queryset = models.Campaign.objects.by_country(country, **params)
                if queryset.exists():
                    # obtém uma campanha aleatória que contém o país informado.
                    obj = queryset.order_by('?')[0]
                    self.check_object_permissions(self.request, obj)
                    logger.debug('Campanha obtida pela prioridade 2 - país')
                    return obj

            # prioridade 1: Uma campanha geral que não se enquadre nos parâmetros anteriores.
            queryset = models.Campaign.objects.general_campaigns(**params)
            obj = queryset.order_by('?')[0]
            self.check_object_permissions(self.request, obj)
            logger.debug('Campanha obtida pela prioridade 1 - geral')
            return obj

        except IndexError:
            raise http.Http404()

    def retrieve(self, request, *args, **kwargs):
        """Ao obter a campanha, imediatamente atribui ganho ao usuário."""
        campaign = self.get_object()
        if not campaign.is_valid_campaign():
            raise http.Http404()

        models.UserCampaignViewLog.objects.view_campaign(user=request.user, campaign=campaign)
        serializer = self.get_serializer(campaign)
        return response.Response(serializer.data)


show_campaign = ShowCampaignView.as_view()


class ViewCampaignView(generics.CreateAPIView):
    """
    Atualiza uma campanha quando o usuário desloqueia sua tela no celular (cadeado). Requer token.

    Esta API é utilizada quando o usuário desloqueia sua tela de celular utilizando o "cadeado". Nesse caso,
    ela diminui o número de visualizações da campanha em 1 e atribui +1 pontos para o usuário. Também registra
    qual usuário visualizou determinada campanha junto com o horário.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.ViewCampaignSerializer
    queryset = models.UserCampaignViewLog.objects.all()
    pagination_class = None
    filter_backends = None

    def perform_create(self, serializer):
        """Passa o usuário logado para o serializador."""
        serializer.save(user=self.request.user)


view_campaign = ViewCampaignView.as_view()


class ViewCampaignRedirectView(ViewCampaignView):
    """
    Atualiza uma campanha quando o usuário desloqueia sua tela no celular (bússola). Requer token.

    Esta API é utilizada quando o usuário desloqueia sua tela de celular utilizando a "bússola". Nesse caso,
    ela diminui o número de visualizações da campanha em 1 e atribui +10 pontos para o usuário. Também
    registra qual usuário visualizou determinada campanha junto com o horário. Retorna a URL
    da propaganda para que o usuário possa visualizá-la no navegador do celular (implementação no mobile).
    """

    serializer_class = serializers.ViewCampaignRedirectSerializer

    def get(self, request, *args, **kwargs):
        """Possibilita o uso da api utilizando GET além de POST."""
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


view_campaign_redirect = ViewCampaignRedirectView.as_view()


class CampaignViewersView(generics.ListAPIView):
    """
    Obtém a list de usuários que visualizaram determinada campanha.

    Permissão de visualização apenas ao dono da campanha ou admin.

    (GET) É possível controlar a paginação utilizando page (qual página visualizar),
    page_size (total por página) e ordering (campo para ordenação). O parâmetro search é utilizado para
    filtrar resultados pelo nome do usuário.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.CampaignViewersSerializer
    queryset = models.UserCampaignViewLog.objects.all()
    pagination_class = CorePaginator
    search_fields = ['user__username']
    filter_fields = ['interaction_type']

    def get_queryset(self):
        """Queryset."""
        try:
            campaign = shortcuts.get_object_or_404(models.Campaign, pk=self.kwargs.get('pk', None))
            if self.request.user.is_superuser:
                return super().get_queryset().filter(campaign=campaign)

            else:
                return super().get_queryset().filter(campaign=campaign, campaign__owner=self.request.user)

        except Exception:
            return super().get_queryset().none()


campaign_viewers = CampaignViewersView.as_view()


class CampaignViewersChartView(generics.GenericAPIView):
    """Obtém o total de visualizações por mês de uma campanha."""

    permission_classes = (permissions.IsCampaignOwnerOrAdmin,)
    serializer_class = serializers.CampaignViewersChartSerializer
    queryset = models.UserCampaignViewLog.objects.all()
    pagination_class = None

    def get(self, request, *args, **kwargs):
        """Get request."""
        try:
            campaign = shortcuts.get_object_or_404(models.Campaign, pk=int(kwargs['pk']))
            params = dict(campaign=campaign.pk,
                          view_date__range=(campaign.start_date, campaign.end_date))

            interaction_type = request.query_params.get('type', None)
            if interaction_type:
                params['interaction_type'] = interaction_type

            if not request.user.is_superuser:
                params['campaign__owner'] = request.user

            month = functions.Trunc('view_date', 'month')
            total = Count('view_date')
            queryset = models.UserCampaignViewLog.objects.filter(**params) \
                                                         .annotate(month=month) \
                                                         .values('month') \
                                                         .annotate(total=total) \
                                                         .order_by('month')

            serializer = self.get_serializer(queryset, many=True)
            return response.Response(serializer.data)

        except KeyError as e:
            raise http.Http404() from e


campaign_viewers_chart = CampaignViewersChartView.as_view()


class ListCampaignsTemplate(generic.TemplateView):
    """Server side partial para campanhas."""

    template_name = 'campaigns/list_campaigns_template.html'


list_campaigns_template = ListCampaignsTemplate.as_view()


class NewCampaignTemplate(generic.TemplateView):
    """Server side partial para criação de campanhas."""

    template_name = 'campaigns/new_campaign_template.html'


new_campaign_template = NewCampaignTemplate.as_view()


class CampaignDetailTemplate(generic.TemplateView):
    """Server side campaign detail partial."""

    template_name = 'campaigns/campaign_detail_template.html'


campaign_detail_template = CampaignDetailTemplate.as_view()
