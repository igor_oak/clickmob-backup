"""Administração da aplicação campaigns."""

from django.contrib import admin

from . import models


@admin.register(models.Campaign)
class CampaignAdmin(admin.ModelAdmin):
    """Campaign admin."""


@admin.register(models.UserCampaignViewLog)
class UserCampaignViewLogAdmin(admin.ModelAdmin):
    """UserCampaignViewLog."""
