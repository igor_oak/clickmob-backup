"""Configuração da aplicação campaigns."""

from django.apps import AppConfig


class CampaignsConfig(AppConfig):
    """Configura a aplicação."""

    name = 'campaigns'
