"""Testa as views da aplicação campaigns."""

import datetime

from core import utils
from django.contrib import auth
from django.core import urlresolvers
from django.core.files import uploadedfile
from django.utils import timezone
from rest_framework import status, test
import base64

from .. import models


class CampaignsTest(test.APITestCase):
    """Verifica as views relacionadas com campanhas."""

    @classmethod
    def setUpTestData(cls):
        """Class fixtures."""
        super().setUpTestData()
        cls.User = auth.get_user_model()
        cls.user1 = cls.User.objects.create_user(
            'user1', 'user1@gmail.com', 'pass123', n_views=1000, user_type=1)
        cls.user2 = cls.User.objects.create_user(
            'user2', 'user2@gmail.com', 'pass123', n_views=1000, user_type=1)

        cls.show_campaign_url = urlresolvers.reverse('campaigns:show_campaign')
        cls.list_campaigns_url = urlresolvers.reverse('campaigns:list_campaigns')

        cls.view_campaign_redirect_url = urlresolvers.reverse('campaigns:view_campaign_redirect')

        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            cls.campaign1 = models.Campaign.objects.create(
                title='Campaign1',
                url='http://no-where.org',
                owner=cls.user1,
                initial_n_views=100,
                photo=uploadedfile.UploadedFile(file, name='python.png', content_type='img/png'),
                is_active=True,
                cities=['Cidade'],
                start_date=timezone.make_aware(datetime.datetime(2016, 1, 1)),
                end_date=timezone.make_aware(datetime.datetime(2020, 2, 2)),
            )
            cls.campaign2 = models.Campaign.objects.create(
                title='Campaign2',
                url='http://no-where.org',
                owner=cls.user1,
                initial_n_views=100,
                photo=uploadedfile.UploadedFile(file, name='python.png', content_type='img/png'),
                cities=['Cidade'],
                start_date=timezone.make_aware(datetime.datetime(2016, 1, 1)),
                end_date=timezone.make_aware(datetime.datetime(2020, 2, 2)),
            )

    def test_user_campaign_create(self):
        """Verifica a API para criação de campanhas."""
        self.assertEqual(models.Campaign.objects.count(), 2)

        self.client.login(username='user1', password='pass123')
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            data = dict(
                title='Campaign3',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                is_active=True,
                cities=['Cidade'],
                start_date=timezone.make_aware(datetime.datetime(2016, 1, 3)),
                end_date=timezone.make_aware(datetime.datetime(2020, 2, 2)),
            )
            response = self.client.post(self.list_campaigns_url, data)
            self.assertTrue(status.is_success(response.status_code))
            self.assertEqual(models.Campaign.objects.count(), 3)

    def test_campaign_unlock(self):
        """
        Verifica se ganhos são atribuidos ao usuário e a campanha é atualizada no desbloqueio de tela.

        Desbloqueio normal (cadeado).
        """
        self.assertEqual(self.user2.user_points, 0)
        self.assertEqual(self.campaign1.n_views, 100)
        self.assertEqual(models.UserCampaignViewLog.objects.count(), 0)

        self.client.login(username='user2', password='pass123')
        response = self.client.get(self.show_campaign_url, dict(campaign=self.campaign1.pk))
        self.assertTrue(status.is_success(response.status_code))

        user2 = self.User.objects.get(pk=self.user2.pk)
        self.assertEqual(user2.user_points, 1)
        self.assertEqual(user2.campaigns_viewed.count(), 1)

        campaign1 = models.Campaign.objects.get(pk=self.campaign1.pk)
        self.assertEqual(campaign1.n_views, 99)
        self.assertEqual(campaign1.viewers.count(), 1)

        self.assertEqual(models.UserCampaignViewLog.objects.count(), 1)

        response = self.client.get(self.show_campaign_url, dict(campaign=self.campaign1.pk))
        self.assertTrue(status.is_success(response.status_code))

        user2 = self.User.objects.get(pk=self.user2.pk)
        self.assertEqual(user2.user_points, 2)
        self.assertEqual(user2.campaigns_viewed.count(), 2)

        campaign1 = models.Campaign.objects.get(pk=self.campaign1.pk)
        self.assertEqual(campaign1.n_views, 98)
        self.assertEqual(campaign1.viewers.count(), 2)

        self.assertEqual(models.UserCampaignViewLog.objects.count(), 2)

    def test_campaign_unlock_compass(self):
        """
        Verifica se ganhos são atribuidos ao usuário e a campanha é atualizada no desbloqueio de tela.

        Desbloqueio com redirect (bússola).
        """
        self.assertEqual(self.user2.user_points, 0)
        self.assertEqual(self.campaign1.n_views, 100)
        self.assertEqual(models.UserCampaignViewLog.objects.count(), 0)

        self.client.login(username='user2', password='pass123')
        response = self.client.get(self.view_campaign_redirect_url, dict(campaign=self.campaign1.pk))
        self.assertTrue(status.is_success(response.status_code))

        self.assertEqual(response.data['url'], self.campaign1.url)
        self.assertEqual(self.User.objects.get(pk=self.user2.pk).user_points, 10)
        self.assertEqual(models.Campaign.objects.get(pk=self.campaign1.pk).n_views, 99)

        self.assertEqual(models.UserCampaignViewLog.objects.count(), 1)

        response = self.client.get(self.view_campaign_redirect_url, dict(campaign=self.campaign1.pk))
        self.assertTrue(status.is_success(response.status_code))

        self.assertEqual(self.User.objects.get(pk=self.user2.pk).user_points, 20)
        self.assertEqual(models.Campaign.objects.get(pk=self.campaign1.pk).n_views, 98)

        self.assertEqual(models.UserCampaignViewLog.objects.count(), 2)

    def test_invalid_campaign(self):
        """Verifica se a aplicação rejeita requests para campanhas inválidas."""
        self.client.login(username='user2', password='pass123')
        response = self.client.post(self.view_campaign_redirect_url, dict(campaign=self.campaign2.pk))
        self.assertTrue(status.is_client_error(response.status_code))

    def test_campaign_fields(self):
        """Verifica se os campos de exibição são exibidos corretamente em função do usuário."""
        self.client.login(username='user1', password='pass123')

        result = self.client.get(self.list_campaigns_url).data['results'][0]
        self.assertTrue('n_views' in result)
        self.assertTrue('status' in result)

        self.client.login(username='user2', password='pass123')
        response = self.client.get(self.show_campaign_url)
        self.assertFalse('n_views' in response.data)
        self.assertFalse('status' in response.data)


class CampaignShowTest(test.APITestCase):
    """Verifica o algoritmo de busca de campanhas."""

    @classmethod
    def setUpTestData(cls):
        """Class fixtures."""
        super().setUpTestData()
        cls.User = auth.get_user_model()
        cls.user1 = cls.User.objects.create_user(
            'user1', 'user1@gmail.com', 'pass1234', n_views=1000, user_type=1)
        cls.user2 = cls.User.objects.create_user(
            'user2', 'user2@gmail.com', 'pass1234', n_views=1000, user_type=1)
        cls.admin = cls.User.objects.create_superuser('admin', 'admin@gmail.com', 'admin1234')

        cls.show_campaign_url = urlresolvers.reverse('campaigns:show_campaign')
        cls.list_campaigns_url = urlresolvers.reverse('campaigns:list_campaigns')

    def test_campaign_show_priority_5(self):
        """Verifica se uma campanha é selecionada corretamente para uma correspondência lat/lng."""
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            campaign1 = dict(
                title='Campaign1',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                center=dict(latitude=-4.82826004687085, longitude=-47.94433568750003),
                radius=728343.877652038,
                start_date=timezone.make_aware(datetime.datetime.now()),
                end_date=timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=10)),
            )
            self.client.login(username='user1', password='pass1234')
            response = self.client.post(self.list_campaigns_url, campaign1, format='json')
            self.assertTrue(status.is_success(response.status_code))

        activate_campaign_url = urlresolvers.reverse('campaigns:activate_campaign',
                                                     args=[response.data['id']])
        self.client.login(username='admin', password='admin1234')
        data = dict(is_active=True, status='Ativa')
        response = self.client.patch(activate_campaign_url, data, format='json')
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='user2', password='pass1234')
        data = dict(lat=-4.82826004687085, lng=-47.94433568750003)
        response = self.client.get(self.show_campaign_url, data)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(response.data['id'], models.Campaign.objects.get().id)

    def test_campaign_show_priority_4(self):
        """Verifica se uma campanha é selecionada corretamente pela cidade informada."""
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            campaign1 = dict(
                title='Campaign1',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                cities=['Ananindeua', 'Guarulhos'],
                start_date=timezone.make_aware(datetime.datetime.now()),
                end_date=timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=10)),
            )
            self.client.login(username='user1', password='pass1234')
            response = self.client.post(self.list_campaigns_url, campaign1, format='json')
            self.assertTrue(status.is_success(response.status_code))

        activate_campaign_url = urlresolvers.reverse('campaigns:activate_campaign',
                                                     args=[response.data['id']])
        self.client.login(username='admin', password='admin1234')
        data = dict(is_active=True, status='Ativa')
        response = self.client.patch(activate_campaign_url, data, format='json')
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='user2', password='pass1234')
        data = dict(city='Guarulhos')
        response = self.client.get(self.show_campaign_url, data)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(response.data['id'], models.Campaign.objects.get().id)

    def test_campaign_show_priority_3(self):
        """Verifica se uma campanha é selecionada corretamente pelo estado informado."""
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            campaign1 = dict(
                title='Campaign1',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                states=['PA', 'SP'],
                start_date=timezone.make_aware(datetime.datetime.now()),
                end_date=timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=10)),
            )
            self.client.login(username='user1', password='pass1234')
            response = self.client.post(self.list_campaigns_url, campaign1, format='json')
            self.assertTrue(status.is_success(response.status_code))

        activate_campaign_url = urlresolvers.reverse('campaigns:activate_campaign',
                                                     args=[response.data['id']])
        self.client.login(username='admin', password='admin1234')
        data = dict(is_active=True, status='Ativa')
        response = self.client.patch(activate_campaign_url, data, format='json')
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='user2', password='pass1234')
        data = dict(state='PA')
        response = self.client.get(self.show_campaign_url, data)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(response.data['id'], models.Campaign.objects.get().id)

    def test_campaign_show_priority_2(self):
        """Verifica se uma campanha é selecionada corretamente pelo país informado."""
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            campaign1 = dict(
                title='Campaign1',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                states=['PA', 'SP'],
                start_date=timezone.make_aware(datetime.datetime.now()),
                end_date=timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=10)),
            )
            self.client.login(username='user1', password='pass1234')
            response = self.client.post(self.list_campaigns_url, campaign1, format='json')
            self.assertTrue(status.is_success(response.status_code))

        activate_campaign_url = urlresolvers.reverse('campaigns:activate_campaign',
                                                     args=[response.data['id']])
        self.client.login(username='admin', password='admin1234')
        data = dict(is_active=True, status='Ativa')
        response = self.client.patch(activate_campaign_url, data, format='json')
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='user2', password='pass1234')
        data = dict(country='Brasil')
        response = self.client.get(self.show_campaign_url, data)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(response.data['id'], models.Campaign.objects.get().id)

    def test_campaign_show_priority_1(self):
        """Verifica se uma campanha é selecionada aleatoriamente."""
        with open(utils.get_path_from_dir('./fixtures/python.png', __file__), 'rb') as file:
            campaign1 = dict(
                title='Campaign1',
                url='http://no-where.org',
                initial_n_views=100,
                photo='data:image/png;base64,{}'.format(base64.b64encode(file.read()).decode('utf-8')),
                states=['SP'],
                start_date=timezone.make_aware(datetime.datetime.now()),
                end_date=timezone.make_aware(datetime.datetime.now() + datetime.timedelta(days=10)),
            )
            self.client.login(username='user1', password='pass1234')
            response = self.client.post(self.list_campaigns_url, campaign1, format='json')
            self.assertTrue(status.is_success(response.status_code))

        activate_campaign_url = urlresolvers.reverse('campaigns:activate_campaign',
                                                     args=[response.data['id']])
        self.client.login(username='admin', password='admin1234')
        data = dict(is_active=True, status='Ativa')
        response = self.client.patch(activate_campaign_url, data, format='json')
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='user2', password='pass1234')
        data = dict(lat=-4.82826004687085, lng=-47.94433568750003, city='Guarulhos', state='PA')
        response = self.client.get(self.show_campaign_url, data)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(response.data['id'], models.Campaign.objects.get().id)
