"""Modelos da aplicação campaigns."""

from core.models import gender_choices
from django import dispatch
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.postgres import fields
from django.db.models import signals
from django.utils import timezone
from model_utils.models import TimeStampedModel
from rest_framework import exceptions

from . import managers


def user_campaigns_images(instance, filename):
    """Define o diretório onde as imagens de uma campanha do usuário serão salvas."""
    return 'clickmob/campaigns/{}/{}/{}'.format(
        instance.owner.username,
        instance.title,
        filename
    )


class Campaign(TimeStampedModel, models.Model):
    """Uma campanha (ads) criada pelo usuário."""

    status_choices = [('Em Avaliação', 'Em Avaliação'), ('Ativa', 'Ativa'), ('Finalizada', 'Finalizada')]

    title = models.CharField(verbose_name='Título', max_length=50, unique=True)
    url = models.URLField()
    photo = models.ImageField(upload_to=user_campaigns_images)
    initial_n_views = models.PositiveIntegerField()
    n_views = models.PositiveIntegerField()
    status = models.CharField(max_length=20, choices=status_choices, default='Em Avaliação')
    is_active = models.BooleanField(default=False)
    is_canceled = models.BooleanField(default=False)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='campaigns')
    viewers = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                     related_name='campaigns_viewed',
                                     through='UserCampaignViewLog')

    states = fields.ArrayField(models.CharField(max_length=2), blank=True, default=[])
    cities = fields.ArrayField(models.CharField(max_length=30), blank=True, default=[])
    countries = fields.ArrayField(models.CharField(max_length=30), blank=True, default=['Brasil'])
    center = models.PointField(srid=4326, null=True, blank=True)
    radius = models.FloatField(null=True, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    gender = models.CharField(max_length=2, choices=gender_choices, blank=True)
    age = models.PositiveIntegerField(default=0)

    objects = managers.GeoCampaignManager()

    def is_valid_campaign(self):
        """Verifica se a campanha é válida em função de seus atributos."""
        now = timezone.now()
        return (self.is_active and not self.is_canceled and self.n_views >= 1 and self.start_date <= now and
                self.end_date >= now)

    def cancel_campaign(self):
        """Cancela, ou reativa a campanha, e atualiza as visualizações do usuário de acordo."""
        if self.n_views == 0:
            raise exceptions.ValidationError({
                'n_views': 'Uma campanha finalizada não pode ser cancelada.'
            })

        self.is_canceled = not self.is_canceled

        user = self.owner
        if self.is_canceled:
            user.n_views += self.n_views

        else:
            if (user.n_views - self.n_views) < 0:
                msg = 'Você não possui visualizações suficientes para reativar essa campanha. ' \
                      'Considere adquirir algum de nossos packs. Obrigado.'
                raise exceptions.ValidationError({
                    'n_views': msg
                })

            user.n_views -= self.n_views

        self.save()
        user.save()

    def save(self, *args, **kwargs):
        """Atualiza o número de views campanha para seu valor inicial."""
        if self.pk is None:
            self.n_views = self.initial_n_views

        return super().save(*args, **kwargs)

    def __str__(self):
        """toString."""
        return self.title

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Campanha'


class UserCampaignViewLog(models.Model):
    """Registra a visualização da campanha por um usuário."""

    interaction_type_choices = [('Visualização', 'Visualização'), ('Interação', 'Interação')]

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    campaign = models.ForeignKey(Campaign)
    view_date = models.DateTimeField(default=timezone.now)
    interaction_type = models.CharField(max_length=20, default='clique', choices=interaction_type_choices)

    objects = managers.UserCampaignViewManager()

    def __str__(self):
        """toString."""
        return '[Usuário={}][Campanha={}][Data={}]'.format(self.user.username,
                                                           self.campaign.title,
                                                           self.view_date)

    class Meta:
        ordering = ('-view_date',)


@dispatch.receiver(signals.post_save, sender=Campaign)
def update_owner_n_views_decrease(sender, instance, **kwargs):
    """Atualiza o número de views que o usuário possui após criar a campanha."""
    if kwargs.get('created', False):
        instance.owner.n_views -= instance.n_views
        instance.owner.save()
