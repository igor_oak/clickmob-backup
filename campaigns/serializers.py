"""Serializadores da aplicação campaigns."""

from core.serializers import UserSerializer
from django import http
from drf_extra_fields.fields import Base64ImageField
from drf_extra_fields.geo_fields import PointField
from rest_framework import exceptions, serializers

from . import models


class CampaignSerializer(serializers.ModelSerializer):
    """Serializador de campaigns."""

    center = PointField(required=False)
    photo = Base64ImageField()
    owner = UserSerializer(read_only=True)

    def validate_initial_n_views(self, n_views):
        """Verifica se o usuário possui visualizações suficientes para criar a campanha."""
        user = self.context['request'].user
        if (user.n_views - n_views) < 0:
            msg = 'Você não possui visualizações suficientes para criar essa campanha. Considere adquirir ' \
                  'algum de nossos packs. Obrigado.'
            raise exceptions.ValidationError(msg)

        return n_views

    def validate(self, data):
        """Verifica se dados válidos estão corretos."""
        if data['start_date'] >= data['end_date']:
            raise exceptions.ValidationError({
                'end_date': 'Data final é menor ou igual que a data incial.'
            })

        if not (data.get('states', None) or
                data.get('cities', None) or
                (data.get('center', None) and data.get('radius', None))):
            raise exceptions.ValidationError({
                'scope': 'Pelo menos um tipo de abrangência precisa ser selecionada.'
            })

        return data

    def to_representation(self, campaign):
        """Atualiza os campos de exibição em função do dono do recurso."""
        campaign_dict = super().to_representation(campaign)
        if self.context['request'].user != campaign.owner and not self.context['request'].user.is_superuser:
            campaign_dict = dict(id=campaign_dict['id'],
                                 url=campaign_dict['url'],
                                 photo=campaign_dict['photo'])

        return campaign_dict

    class Meta:
        model = models.Campaign
        fields = ['id', 'title', 'photo', 'states', 'cities', 'n_views', 'status', 'created', 'url',
                  'start_date', 'end_date', 'center', 'radius', 'gender', 'age', 'owner', 'is_active',
                  'is_canceled', 'initial_n_views']
        read_only_fields = ['status', 'is_active', 'n_views']


class CancelCampaignSerializer(serializers.ModelSerializer):
    """Serializador utilizado quando o usuário precisa cancelar uma campanha."""

    n_views = serializers.ReadOnlyField(source='owner.n_views')

    def save(self, **kwargs):
        """Cancela, ou reativa, a campanha atualizando as views restantes para o usuário."""
        campaign = self.instance
        campaign.cancel_campaign()

    class Meta:
        model = models.Campaign
        fields = ['id', 'is_canceled', 'n_views']
        read_only_fields = ['is_canceled']


class AdminActivateCampaignSerializer(serializers.ModelSerializer):
    """Serializador de campanhas para o admin."""

    class Meta:
        model = models.Campaign
        fields = ['id', 'status', 'is_active']


class ViewCampaignSerializer(serializers.ModelSerializer):
    """Atualiza uma campanha."""

    campaign = serializers.PrimaryKeyRelatedField(
        queryset=models.Campaign.objects.all(),
        write_only=True
    )

    def validate_campaign(self, campaign):
        """Verifica se a campanha é válida."""
        if campaign.is_valid_campaign():
            return campaign

        raise http.Http404()

    def save(self, **kwargs):
        """Atualiza uma campanha, diminuindo seu número de views e atribuindo ganhos ao usuário."""
        user = kwargs['user']
        campaign = self.validated_data['campaign']
        models.UserCampaignViewLog.objects.view_campaign(user=user, campaign=campaign)

    class Meta:
        model = models.UserCampaignViewLog
        fields = ['campaign']


class ViewCampaignRedirectSerializer(ViewCampaignSerializer):
    """Atualiza uma campanha."""

    url = serializers.URLField(read_only=True, source='campaign.url')

    def save(self, **kwargs):
        """Atualiza uma campanha, diminuindo seu número de views e atribuindo ganhos ao usuário."""
        user = kwargs['user']
        campaign = self.validated_data['campaign']
        models.UserCampaignViewLog.objects.view_campaign_redirect(user=user, campaign=campaign)

    class Meta(ViewCampaignSerializer.Meta):
        fields = ViewCampaignSerializer.Meta.fields + ['url']


class CampaignViewersSerializer(serializers.ModelSerializer):
    """Serializador de registros de visualização de uma campanha."""

    username = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = models.UserCampaignViewLog
        fields = ['id', 'username', 'view_date', 'interaction_type']
        read_only_fields = ['view_date', 'interaction_type']


class CampaignViewersChartSerializer(serializers.Serializer):
    """Serializador para alimentar o gráfico de viewers."""

    total = serializers.IntegerField()
    month = serializers.DateTimeField()


class CampaignDetailSerializer(serializers.ModelSerializer):
    """Serializador para detalhes de uma campanha."""

    center = PointField(required=False)

    class Meta:
        model = models.Campaign
        fields = ['id', 'title', 'photo', 'states', 'cities', 'n_views', 'status', 'created', 'url',
                  'start_date', 'end_date', 'center', 'radius', 'gender', 'age', 'owner', 'is_active',
                  'is_canceled', 'initial_n_views']
