"""Permissões da aplicação campaigns."""

from rest_framework import permissions


class IsCampaignOwnerOrAdmin(permissions.BasePermission):
    """Acesso apenas ao dono do recurso ou o Admin."""

    def has_object_permission(self, request, view, obj):
        """Verifica a permissão ao objeto."""
        return request.user.is_superuser or request.user == obj.owner

    def has_permission(self, request, view):
        """Verifica a permissão."""
        return request.user.is_superuser or request.user.is_authenticated() and request.user.user_type >= 1
