"""Amazon staging wsgi module."""

import os

from django.core import wsgi

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "clickmob.settings.amazon_staging")
application = wsgi.get_wsgi_application()
