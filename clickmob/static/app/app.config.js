(function () {
  'use strict';

  angular.module('clickmob')
    .config(['$interpolateProvider', interpolateConfig])
    .config(['$resourceProvider', resourceConfig])
    .config(['uiGmapGoogleMapApiProvider', 'gmapsKey', uiGmapsConfig])
    .config(['$httpProvider', httpConfig]);

  function interpolateConfig($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  }

  function resourceConfig($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
  }

  function uiGmapsConfig(uiGmapGoogleMapApiProvider, gmapsKey) {
    uiGmapGoogleMapApiProvider.configure({
      key: gmapsKey,
      libraries: 'geometry,visualization'
    });
  }

  function httpConfig($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  }
})();
