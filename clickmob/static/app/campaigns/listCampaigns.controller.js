(function () {
  'use strict';

  angular.module('clickmob').controller('listCampaignsCtrl', [
    '$http', '$rootScope', 'Campaign', 'Lightbox', 'toastr', controller
  ]);

  function controller($http, $rootScope, Campaign, Lightbox, toastr) {
    var self = this;
    self.page_size = 10;
    self.query = '';
    self.loadCampaigns = loadCampaigns;
    self.openImage = openImage;
    self.updateCampaign = updateCampaign;
    self.cancelCampaign = cancelCampaign;

    self.$onInit = function () {
      loadCampaigns({page: 1, page_size: self.page_size});
    }

    function loadCampaigns(params) {
      self.promise = Campaign.query(params, success).$promise;

      function success(campaigns) {
        self.campaigns = campaigns;
      }
    }

    function openImage(obj) {
      Lightbox.openModal([obj], 0);
    }

    function updateCampaign(campaign, data) {
      var message = campaign.is_active ? 'Deseja desativar essa campanha?' : 'Confirma ativação desta campanha?';
      if (confirm(message)) {
        self.promise = Campaign.activate(data, success, error);

        function success(data) {
          campaign.status = data.status;
          campaign.is_active = data.is_active;
          toastr.success('Campanha atualizada com sucesso.');
        }

        function error(response) {
          console.log(response);
        }
      }
    }

    function cancelCampaign(campaign) {
      var _campaign = angular.copy(campaign);
      var message = 'Deseja cancelar essa campanha? Nesse caso ela ficará indisponível para visualização mas você receberá as visualizações restantes para reutilizar.';
      if (_campaign.is_canceled) {
        message = 'Deseja reativar essa campanha? Você precisa ter o mesmo número de visualizações disponíveis que campanha tinha anteriormente para investir na campanha.';
      }

      if (confirm(message)) {
        self.promise = _campaign.$cancel(success, error);

        function success(data) {
          campaign.is_canceled = data.is_canceled;
          $rootScope.User.n_views = data.n_views;
          toastr.success('Campanha atualizada com sucesso.');
        }

        function error(response) {
          alert(response.data.n_views);
        }
      }
    }
  }
})();
