(function () {
  'use strict';

  angular.module('clickmob').controller('newCampaignCtrl', [
    '$rootScope', '$scope', '$window', 'Campaign', 'toastr', 'statesMap', 'uiGmapGoogleMapApi', controller
  ]);

  function controller($rootScope, $scope, $window, Campaign, toastr, statesMap, uiGmapGoogleMapApi) {
    var self = this;
    self.$onInit = init;

    function init() {
      self.campaign = {};
      self.campaignType = '';
      self.campaign.photo_file = new File([], '');

      /* gmaps */
      self.campaignMap = {
        center: {latitude: -8.754795, longitude: -52.910156},
        zoom: 6
      };

      self.campaignCenter = {
        id: 1,
        position: {
          latitude: -8.754795,
          longitude: -52.910156
        },
        options: {
          draggable: true,
          label: 'Origem da campanha'
        },
        events: {
          drag: updateCircle
        }
      };

      self.campaignLimit = {
        id: 2,
        position: {
          latitude: -7,
          longitude: -52.910156
        },
        options: {
          draggable: true,
          label: 'Limite da campanha'
        },
        events: {
          drag: updateCircle
        }
      };

      self.circle = {
        stroke: {
          color: '#08B21F',
          weight: 2,
          opacity: 1
        },
        fill: {
          color: '#08B21F',
          opacity: 0.5
        }
      };

      function updateCircle() {
        uiGmapGoogleMapApi.then(function(maps) {
          self.circle.radius = maps.geometry.spherical.computeDistanceBetween(
            new maps.LatLng(self.campaignCenter.position.latitude, self.campaignCenter.position.longitude),
            new maps.LatLng(self.campaignLimit.position.latitude, self.campaignLimit.position.longitude)
          );
        });
      }

      updateCircle();

      initStartDate();
      updateStartDate();
      initEndDate();
      updateEndDate();
    }

    self.statesMap = statesMap;
    self.updateStartDate = updateStartDate;
    self.updateEndDate = updateEndDate;
    self.changeCampaignType = changeCampaignType;
    self.showCities = showCities;
    self.addCities = addCities;
    self.removeCities = removeCities;
    self.createCampaign = createCampaign;

    function initStartDate() {
      self.campaign.start_date_date = new Date();
      self.campaign.start_date_time = new Date();
      self.campaign.start_date_time.setHours(0);
      self.campaign.start_date_time.setMinutes(0);
    }

    function initEndDate() {
      self.campaign.end_date_date = new Date();
      self.campaign.end_date_time = new Date();
      self.campaign.end_date_time.setHours(0);
      self.campaign.end_date_time.setMinutes(0);
    }

    function updateStartDate() {
      self.campaign.start_date = new Date(
        self.campaign.start_date_date.getFullYear(),
        self.campaign.start_date_date.getMonth(),
        self.campaign.start_date_date.getDate(),
        self.campaign.start_date_time.getHours(),
        self.campaign.start_date_time.getMinutes(),
        0
      ).toISOString();
    }

    function updateEndDate () {
      self.campaign.end_date = new Date(
        self.campaign.end_date_date.getFullYear(),
        self.campaign.end_date_date.getMonth(),
        self.campaign.end_date_date.getDate(),
        self.campaign.end_date_time.getHours(),
        self.campaign.end_date_time.getMinutes(),
        0
      ).toISOString();
    }

    function changeCampaignType() {
      self.campaign.states = [];
      self.campaign.cities = [];
      self.cities = [];
      self.stateCity = '';
      self.selectedCities = [];
    }

    function showCities() {
      self.cities = self.statesMap[self.stateCity].cidades;
    }

    function addCities() {
      for (var i = 0; i < self.selectedCities.length; i++) {
        if (self.campaign.cities.indexOf(self.selectedCities[i]) === -1) {
          self.campaign.cities.push(self.selectedCities[i]);
        }
      }
      self.campaign.cities.sort();
    }

    function removeCities() {
      for (var i = 0; i < self.selectedCitiesRemove.length; i++) {
        var index = self.campaign.cities.indexOf(self.selectedCitiesRemove[i]);
        if (index !== -1) {
          self.campaign.cities.splice(index, 1);
        }
      }
    }

    function createCampaign() {
      self.errors = {};

      if (self.campaignType == '2') {
        self.campaign.center = self.campaignCenter.position;
        self.campaign.radius = self.circle.radius;
      }

      var reader = new FileReader();
      reader.onload = function () {
        $scope.$apply(function() {
          self.campaign.photo = reader.result;
          self.promise = new Campaign(self.campaign).$save(success, error).$promise;

          function success(campaign) {
            toastr.success('Campanha criada com sucesso.');
            init();
            $(":file").filestyle('clear');
            $window.scrollTo(0, 0);
            self.showCampaignMessage = true;
            $rootScope.User = campaign.owner;
          }

          function error(response) {
            self.errors = response.data;
          }
        });
      };
      reader.readAsDataURL(self.campaign.photo_file);
    }
  }
})();
