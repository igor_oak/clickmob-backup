(function () {
  'use strict';

  angular.module('clickmob')
    .factory('Campaign', ['$resource', 'CoreUtils', Campaign]);

  function Campaign($resource, CoreUtils) {
    var _Campaign = $resource('/campaigns/:id/', {id: '@id'}, {
      query: {
        method: 'GET',
        isArray: false,
        transformResponse: function (response) {
          return CoreUtils.setUpResource(_Campaign, response);
        }
      },
      update: {
        method: 'PUT'
      },
      cancel: {
        method: 'PUT',
        url: '/campaigns/:id/cancel/',
        params: {id: '@id'}
      },
      viewersLog: {
        method: 'GET',
        url: '/campaigns/:id/viewers/log/',
        params: {id: '@id'}
      },
      chartData: {
        method: 'GET',
        isArray: true,
        url: '/campaigns/:id/chart/data/'
      },
      activate: {
        method: 'PATCH',
        url: '/campaigns/:id/activate/'
      }
    });
    return _Campaign;
  }
})();
