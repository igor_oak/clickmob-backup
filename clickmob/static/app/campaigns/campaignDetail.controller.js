(function () {
  'use strict';

  angular.module('clickmob').controller('campaignDetailCtrl', [
    'campaign', 'Campaign', 'uiGmapGoogleMapApi', controller
  ]);

  function controller(campaign, Campaign, uiGmapGoogleMapApi) {
    var self = this;
    self.page_size = 15;
    self.query = '';
    self.brFormatters = d3.locale({
      "decimal": ".",
      "thousands": ",",
      "grouping": [3],
      "currency": ["R$", ""],
      "dateTime": "%d/%m/%Y %H:%M:%S",
      "date": "%d/%m/%Y",
      "time": "%H:%M:%S",
      "periods": ["AM", "PM"],
      "days": ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      "shortDays": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
      "months": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      "shortMonths": ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
    });
    var now = new Date();
    now.setTime(now.getTime() + now.getTimezoneOffset() * 60000);
    self.year = now.getFullYear();

    self.loadViewersLog = loadViewersLog;
    self.loadChartData = loadChartData;

    self.$onInit = function () {
      self.campaign = campaign;
      self.loadViewersLog({page_size: self.page_size});
      self.loadChartData({id: self.campaign.id});

      if (self.campaign.center && self.campaign.radius) {
        /* gmaps */
        self.campaignMap = {
          center: {latitude: self.campaign.center.latitude, longitude: self.campaign.center.longitude},
          zoom: 4
        };

        self.campaignCenter = {
          id: 1,
          position: {
            latitude: self.campaignMap.center.latitude,
            longitude: self.campaignMap.center.longitude
          },
          options: {
            label: 'Origem da campanha'
          }
        };

        self.circle = {
          stroke: {
            color: '#08B21F',
            weight: 2,
            opacity: 1
          },
          fill: {
            color: '#08B21F',
            opacity: 0.5
          },
          radius: self.campaign.radius
        };
      }
    }

    function loadViewersLog(params) {
      var _campaign = angular.copy(self.campaign);
      self.viewersLogPromise = _campaign.$viewersLog(params, success, error);

      function success(results) {
        self.viewersLog = results;
      }

      function error(response) {
        console.log('error', response);
      }
    }

    function loadChartData(params) {
      self.chartPromise = Campaign.chartData(params, success).$promise;

      function success(results) {
        var chartCount = 0;
        var d;
        for (var i = 0; i < results.length; i++) {
          // d = new Date(results[i].month);
          //d.setTime(d.getTime() + d.getTimezoneOffset() * 60 * 1000);
          results[i].month = new Date(results[i].month);
          chartCount += results[i].total;
        }

        self.chartCount = chartCount;
        self.data = {
          dataset0: results
        };

        self.options = {
          series: [
            {
              axis: "y",
              dataset: "dataset0",
              key: "total",
              label: "Total por mês",
              color: "#1f77b4",
              type: ['column'],
              ticks: 40,
              id: 'mySeries0'
            }
          ],
          axes: {
            x: {
              key: "month",
              type: "date",
              tickFormat: self.brFormatters.timeFormat('%b %Y')
            },
            y: {
              padding: {max: 10}
            }
          }
        };
      }
    }
  }
})();
