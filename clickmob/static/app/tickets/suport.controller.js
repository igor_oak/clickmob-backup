(function () {
  'use strict';

  angular.module('clickmob').controller('suportCtrl', ['Ticket', '$uibModal', 'toastr', controller]);

  function controller(Ticket, $uibModal, toastr) {
    var self = this;
    self.page_size = 5;
    self.loadTickets = loadTickets;
    self.openNewTicketModal = openNewTicketModal;
    self.openEditTicketModal = openEditTicketModal;

    self.$onInit = function () {
      self.query = '';
      loadTickets({page: 1, page_size: self.page_size});
    }

    function loadTickets(data) {
      self.promise = Ticket.query(data, success).$promise;

      function success(tickets) {
        self.tickets = tickets;
      }
    }

    function openNewTicketModal() {
      $uibModal.open({
        templateUrl: 'newTicketModal.html',
        controllerAs: 'vm',
        controller: ['$uibModalInstance', modalController],
        backdrop: 'static'
      }).result.then(function (ticket) {
        self.$onInit();
        toastr.success('Ticket criado com sucesso');
      });

      function modalController($uibModalInstance) {
        var self  = this;
        self.ticket = {};
        this.saveTicket = saveTicket;
        this.close = close;

        function saveTicket() {
          self.createTicketPromise = new Ticket(self.ticket).$save(success).$promise;

          function success(ticket) {
            $uibModalInstance.close(ticket);
          }
        }

        function close() {
          $uibModalInstance.dismiss();
        }
      }
    }

    function openEditTicketModal(ticket) {
      var self = this;
      self.ticket = ticket;

      $uibModal.open({
        templateUrl: 'editTicketModal.html',
        controllerAs: 'vm',
        controller: ['$uibModalInstance', 'ticket', modalController],
        backdrop: 'static',
        resolve: {
          ticket: self.ticket
        }
      }).result.then(function (ticket) {
        for (var i = 0; i < self.tickets.results.length; i++) {
          if (self.tickets.results[i].id === ticket.id) {
            self.tickets.results[i] = ticket;
            break;
          }
        }
        toastr.success('Ticket atualizado com sucesso.');
      });

      function modalController($uibModalInstance, ticket) {
        var self  = this;
        self.ticket = angular.copy(ticket);
        this.updateTicket = updateTicket;
        this.close = close;

        function updateTicket() {
          self.updateTicketPromise = self.ticket.$update(success).$promise;

          function success(ticket) {
            $uibModalInstance.close(ticket);
          }
        }

        function close() {
          $uibModalInstance.dismiss();
        }
      }
    }
  }
})();
