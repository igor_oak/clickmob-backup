(function () {
  'use strict';

  angular.module('clickmob').component('statusDisplay', {
    bindings: {
      status: '@'
    },
    controllerAs: 'vm',
    controller: function () {
      var self = this;
      var statusMap = {
        'Aberto': 'primary',
        'Pendente': 'warning',
        'Em Espera': 'info',
        'Resolvido': 'success',
        'Fechado': 'danger'
      }
      self.class = 'label label-' + statusMap[self.status];
    },
    template: '<span ng-class="vm.class">[[ vm.status ]]</span>'
  })
})();
