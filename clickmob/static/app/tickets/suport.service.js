(function () {
  'use strict';

  angular.module('clickmob')
    .factory('Ticket', ['$resource', 'ticketsURL', Ticket]);

  function Ticket($resource, ticketsURL) {
    var _Ticket = $resource(ticketsURL + ':id/', {id: '@id'}, {
      query: {
        method: 'GET',
        isArray: false,
        transformResponse: function (data) {
          var data = angular.fromJson(data)
          var results = [];
          angular.forEach(data.results, function (ticket) {
            results.push(new _Ticket(ticket));
          });
          data.results = results;
          return data;
        }
      },
      update: {
        method: 'PUT'
      }
    });
    return _Ticket;
  }
})();
