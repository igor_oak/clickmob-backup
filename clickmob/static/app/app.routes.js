(function () {
  'use strict';

  var app = angular.module('clickmob');
  app.config([
    '$stateProvider',
    '$urlRouterProvider',
    'settingsTemplate',
    'suportTemplate',
    'buyPacksTemplate',
    'createPacksTemplate',
    'listCampaignsTemplate',
    'newCampaignTemplate',
    'campaignDetailTemplate',
    'shoppingCartTemplate',
    'orderHistoryTemplate',
    'userListTemplate',
    routeConfig
  ]);

  app.run(['$rootScope', '$state', 'User', function($rootScope, $state, User) {
    $rootScope.$state = $state;
    $rootScope.User = User;
  }]);

  function routeConfig($stateProvider,
                       $urlRouterProvider,
                       settingsTemplate,
                       suportTemplate,
                       buyPacksTemplate,
                       createPacksTemplate,
                       listCampaignsTemplate,
                       newCampaignTemplate,
                       campaignDetailTemplate,
                       shoppingCartTemplate,
                       orderHistoryTemplate,
                       userListTemplate) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('index', {
      url: '/',
      templateUrl: '/static/partials/home.html'
    });

    $stateProvider.state('settings', {
      url: '/settings',
      templateUrl: settingsTemplate,
      controllerAs: 'vm',
      controller: 'settingsCtrl'
    });

    $stateProvider.state('users', {
      url: '/users',
      templateUrl: userListTemplate,
      controllerAs: 'vm',
      controller: 'userListCtrl'
    });

    $stateProvider.state('suport', {
      url: '/suport',
      templateUrl: suportTemplate,
      controllerAs: 'vm',
      controller: 'suportCtrl'
    });

    $stateProvider.state('campaigns', {
      url: '/campaigns',
      abstract: true,
      template: '<div ui-view></div>'
    }).state('campaigns.new', {
      url: '/new',
      templateUrl: newCampaignTemplate,
      controllerAs: 'vm',
      controller: 'newCampaignCtrl'
    }).state('campaigns.list', {
      url: '/list',
      templateUrl: listCampaignsTemplate,
      controllerAs: 'vm',
      controller: 'listCampaignsCtrl'
    }).state('campaigns.detail', {
      url: '/detail/:campaign',
      templateUrl: campaignDetailTemplate,
      controllerAs: 'vm',
      controller: 'campaignDetailCtrl',
      resolve: {
        campaign: function ($stateParams, Campaign) {
          return Campaign.get({id: $stateParams.campaign}).$promise;
        }
      }
    });

    $stateProvider.state('packs', {
      url: '/packs',
      abstract: true,
      template: '<div ui-view></div>'
    }).state('packs.buy', {
      url: '/buy',
      templateUrl: buyPacksTemplate,
      controllerAs: 'vm',
      controller: 'buyPackCtrl'
    }).state('packs.history', {
      url: '/history',
      templateUrl: orderHistoryTemplate,
      controllerAs: 'vm',
      controller: 'orderHistoryCtrl'
    }).state('packs.create', {
      url: '/create',
      templateUrl: createPacksTemplate,
      controllerAs: 'vm',
      controller: 'createPacksCtrl'
    }).state('packs.cart', {
      url: '/cart',
      templateUrl: shoppingCartTemplate,
      controllerAs: 'vm',
      controller: 'OrdersCtrl'
    });
  }
})();
