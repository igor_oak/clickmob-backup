(function () {
  'use strict';

  angular.module('clickmob').controller('createPacksCtrl', ['Pack', '$uibModal', 'toastr', controller]);

  function controller(Pack, $uibModal, toastr) {
    var self = this;
    self.page_size = 5;
    self.loadPacks = loadPacks;
    self.openNewPackModal = openNewPackModal;

    self.$onInit = function () {
      self.query = '';
      self.pack = {};
      loadPacks({page: 1, page_size: self.page_size});
    }

    function loadPacks(params) {
      self.promise = Pack.query(params, success).$promise;

      function success(packs) {
        self.packs = packs;
      }
    }

    function openNewPackModal() {
      $uibModal.open({
        templateUrl: 'newPackModal.html',
        controllerAs: 'vm',
        controller: ['$uibModalInstance', modalController],
        backdrop: 'static'
      }).result.then(function (pack) {
        self.$onInit();
        toastr.success('Pack criado com sucesso');
      });

      function modalController($uibModalInstance) {
        var self  = this;
        self.pack = {};
        this.createPack = createPack;
        this.close = close;

        function createPack() {
          self.errors = {};
          self.createPackPromise = new Pack(self.pack).$save(success).$promise;

          function success(pack) {
            $uibModalInstance.close(pack);
          }

          function error(response) {
            self.errors = response.data;
          }
        }

        function close() {
          $uibModalInstance.dismiss();
        }
      }
    }
  }
})();
