(function () {
  'use strict';

  angular.module('clickmob').factory('Pack', ['$resource', Pack]);

  function Pack($resource) {
    var _Pack = $resource('/packs/list/', null, {
      query: {
        method: 'GET',
        isArray: false,
        transformResponse: function (data) {
          var data = angular.fromJson(data);
          var results = [];
          angular.forEach(data.results, function (pack) {
            results.push(new _Pack(pack));
          });
          data.results = results;
          return data;
        }
      }
    });
    return _Pack;
  }
})();
