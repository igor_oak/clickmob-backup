(function () {
  'use strict';

  angular.module('clickmob').controller('buyPackCtrl', [
    '$rootScope', 'toastr', 'Pack', 'ShoppingCart', controller
  ]);

  function controller($rootScope, toastr, Pack, ShoppingCart) {
    var self = this;
    self.loadPacks = loadPacks;
    self.addPackToCart = addPackToCart;

    self.$onInit = function () {
      loadPacks();
    }

    function loadPacks(params) {
      self.promise = Pack.query(params, success).$promise;

      function success(packs) {
        self.packs = packs;
      }
    }

    function addPackToCart(pack) {
      if (confirm('Deseja adicionar ao carrinho?')) {
        var shopping_cart_items = [{pack: pack.id, quantity: 1}];
        self.promise = ShoppingCart.addPackToCart({shopping_cart_items: shopping_cart_items}, success).$promise;

        function success(shoppingCart) {
          toastr.success('Pack adicionado ao carrinho!');
          $rootScope.User.total_items = shoppingCart.shopping_cart_items.length;
        }
      }
    }
  }
})();
