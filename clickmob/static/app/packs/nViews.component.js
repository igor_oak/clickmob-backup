(function () {
  'use strict';

  angular.module('clickmob').directive('nViews', directive);

  function directive() {
    return {
      template: ['<div class="info-box">',
        '<span class="info-box-icon bg-green">',
          '<i class="fa fa-eye"></i>',
        '</span>',

        '<div class="info-box-content">',
          '<span class="info-box-text">Total de visualizações</span>',
          '<span class="info-box-number">[[ User.n_views ]]</span>',
        '</div>',
      '</div>'].join('')
    }
  }
})();
