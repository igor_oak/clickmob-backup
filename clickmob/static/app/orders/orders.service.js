  (function () {
  'use strict';

  angular.module('clickmob')
    .factory('OrderUtils', [OrderUtils])
    .factory('Order', ['$resource', 'CoreUtils', Order])
    .factory('ShoppingCartItem', ['$resource', 'OrderUtils', ShoppingCartItem])
    .factory('ShoppingCart', ['$resource', 'ShoppingCartItem', 'OrderUtils', ShoppingCart]);

  function OrderUtils() {
    return {
      setUpShoppingCartResource: setUpShoppingCartResource
    }

    function setUpShoppingCartResource(_Resource, response) {
      var objects = angular.fromJson(response);
      var results = [];
      angular.forEach(objects.shopping_cart_items, function (object) {
        results.push(new _Resource(object));
      })
      objects.shopping_cart_items = results;
      return objects;
    }
  }

  function Order($resource, CoreUtils) {
    var _Order = $resource('/orders/', null, {
      query: {
        method: 'GET',
        isArray: false,
        transformResponse: function (response) {
          return CoreUtils.setUpResource(_Order, response);
        }
      }
    });
    return _Order;
  }

  function ShoppingCartItem($resource, OrderUtils) {
    var _ShoppingCartItem = $resource('/orders/shopping/cart/:id/', {id: '@id'}, {
      deleteItem: {
        method: 'DELETE',
        isArray: false,
        transformResponse: function (response) {
          return OrderUtils.setUpShoppingCartResource(_ShoppingCartItem, response);
        }
      }
    });
    return _ShoppingCartItem;
  }

  function ShoppingCart($resource, ShoppingCartItem, OrderUtils) {
    var _ShoppingCart = $resource('/orders/shopping/cart/', {}, {
      addPackToCart: {
        method: 'PUT',
        isArray: false,
        transformResponse: function (response) {
          return OrderUtils.setUpShoppingCartResource(ShoppingCartItem, response);
        }
      },
      loadShoppingCart: {
        method: 'GET',
        isArray: false,
        transformResponse: function (response) {
          return OrderUtils.setUpShoppingCartResource(ShoppingCartItem, response);
        }
      },
      clearShoppingCart: {
        method: 'DELETE'
      }
    });
    return _ShoppingCart;
  }
})();
