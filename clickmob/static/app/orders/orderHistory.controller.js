(function () {
  'use strict';

  angular.module('clickmob').controller('orderHistoryCtrl', ['Order', controller]);

  function controller(Order) {
    var self = this;
    self.query = '';
    self.page_size = 5;
    self.$onInit = onInit;
    self.loadOrders = loadOrders;

    function onInit() {
      loadOrders({page_size: 5});
    }

    function loadOrders(params) {
      self.promise = Order.query(params, success);

      function success(orders) {
        self.orders = orders;
      }
    }
  }
})();
