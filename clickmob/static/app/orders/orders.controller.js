(function () {
  'use strict';

  angular.module('clickmob').controller('OrdersCtrl', ['Order', 'ShoppingCart', 'toastr', '$rootScope', '$window', controller]);

  function controller(Order, ShoppingCart, toastr, $rootScope, $window) {
    var self = this;
    self.$onInit = onInit;
    self.clearShoppingCart = clearShoppingCart;
    self.deleteShoppingCartItem = deleteShoppingCartItem;
    self.updateShoppingCartItem = updateShoppingCartItem;
    self.createOrder = createOrder;

    function onInit() {
      self.promise = ShoppingCart.loadShoppingCart(success).$promise;

      function success(shoppingCart) {
        self.shoppingCart = shoppingCart;
      }
    }

    function clearShoppingCart() {
      if (confirm('Deseja limpar o carrinho?')) {
        self.promise = ShoppingCart.clearShoppingCart(success).$promise;

        function success(shoppingCart) {
          toastr.success('Carrinho esvaziado com sucesso!');
          self.shoppingCart = shoppingCart;
          $rootScope.User.total_items = self.shoppingCart.shopping_cart_items.length;
        }
      }
    }

    function deleteShoppingCartItem(item) {
      if (confirm('Deseja remover esse item do carrinho?')) {
        var _item = angular.copy(item);
        self.promise = _item.$deleteItem(success).$promise;

        function success(shoppingCart) {
          toastr.success('Pack removido do carrinho com sucesso!');
          self.shoppingCart = shoppingCart;
          $rootScope.User.total_items = self.shoppingCart.shopping_cart_items.length;
        }
      }
    }

    function updateShoppingCartItem(message, data) {
      if (confirm(message)) {
        self.promise = ShoppingCart.addPackToCart({shopping_cart_items: data}, success).$promise;

        function success(shoppingCart) {
          toastr.success('Item atualizado com sucesso!');
          self.shoppingCart = shoppingCart;
          $rootScope.User.total_items = self.shoppingCart.shopping_cart_items.length;
        }
      }
    }

    function createOrder() {
      if (confirm('Finaliza a compra?')) {
        self.promise = new Order().$save(success).$promise;

        function success(order) {
          toastr.success('Compra efetuada com sucesso!');
          self.shoppingCart = {
            total_price: 0,
            total_n_views: 0,
            shopping_cart_items: []
          };
          $rootScope.User.total_items = 0;
          $window.open('https://mibankws.azurewebsites.net/api/boleto/exibir-boleto?boleto=' + order.boleto_hash, '_blank');
        }
      }
    }
  }
})();
