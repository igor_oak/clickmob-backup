(function () {
  'use strict';

  angular.module('clickmob').controller('userListCtrl', ['CoreUser', controller]);

  function controller(CoreUser) {
    var self = this;
    self.page_size = 5;
    self.$onInit = onInit;
    self.loadUsers = loadUsers;

    function onInit() {
      self.query = '';
      loadUsers({page_size: self.page_size});
    }

    function loadUsers(params) {
      self.promise = CoreUser.query(params, function (users) {
        self.users = users;
      });
    }
  }
})();
