(function () {
  'use strict';

  angular.module('clickmob').controller('settingsCtrl', [
    '$rootScope', 'statesMap', 'settingsService', 'toastr', controller
  ]);

  function controller($rootScope, statesMap, settingsService, toastr) {
    var self = this;
    self.changePassword = changePassword;
    self.updateUserData = updateUserData;
    self.saveProfileData = saveProfileData;
    self.statesMap = statesMap;
    self.setCities = setCities;
    self.cities = [];

    self.$onInit = function () {
      self.User = angular.copy($rootScope.User);
      if (!self.User.profile) {
        self.User.profile = {};
      }
      self.changePasswordData = {};
      self.states = Object.keys(self.statesMap);
      setCities();
    }

    function changePassword() {
      self.changePasswordErrors = {};
      self.changePasswordPromise = settingsService.changePassword(self.changePasswordData);
      self.changePasswordPromise.then(success, error);

      function success(data) {
        toastr.success('Senha atualizada com sucesso.');
        self.changePasswordData = {};
        self.changePasswordErrors = {};
      }

      function error(response) {
        self.changePasswordErrors = response.data;
      }
    }

    function updateUserData() {
      self.userDataErrors = {};

      if (noFile('id_user_image')) {
        self.User.user_image = '';
      }
      self.updateUserDataPromise = settingsService.updateUserData(self.User);
      self.updateUserDataPromise.then(success, error);

      function success(response) {
        $rootScope.User = response.data;
        self.User = angular.copy($rootScope.User);
        self.User.user_image = '';
        toastr.success('Dados cadastrais atualizados com sucesso.');
      }

      function error(response) {
        self.userDataErrors = response.data;
      }
    }

    function saveProfileData() {
      self.profileDataErrors = {};

      self.createProfilePromise = settingsService.createProfile(self.User.profile);
      self.createProfilePromise.then(success, error);

      function success(response) {
        self.User.profile = response.data;
        $rootScope.User = self.User;
        toastr.success('Informações de Cobrança salvas com sucesso.');
      }

      function error(response) {
        self.profileDataErrors = response.data;
      }
    }

    function setCities() {
      if (self.User.profile.sacado_uf) {
        self.cities = self.statesMap[self.User.profile.sacado_uf].cidades;
      }
    }

    function noFile(id) {
      return document.getElementById(id).files.length == 0;
    }
  }
})();
