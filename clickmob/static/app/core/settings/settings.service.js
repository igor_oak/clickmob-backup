(function () {
  'use strict';

  angular.module('clickmob').factory('settingsService', ['$http', 'userDataURL', 'changePasswordURL', service]);

  function service($http, userDataURL, changePasswordURL) {
    return {
      changePassword: function (data) {
        return $http.post(changePasswordURL, data);
      },
      getUserData: function () {
        return $http.get(userDataURL);
      },
      updateUserData: function (data) {
        return $http.put(userDataURL, data, {
          headers: {
            'Content-Type': undefined
          },
          transformRequest: function (data) {
            var formData = new FormData();
            angular.forEach(data, function (value, key) {
                formData.append(key, value);
            });

            return formData;
          }
        });
      },
      createProfile: function (data) {
        return $http.post('/core/profile/', data);
      }
    }
  }
})();
