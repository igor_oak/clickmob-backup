(function () {
  'use strict';

  angular.module('clickmob').controller('SignupCtrl', ['$uibModal', controller]);

  function controller($uibModal) {
    var self = this;
    self.openFriendModal = openFriendModal;
    self.clearSelectedFriend = clearSelectedFriend;
    self.clearUserImage = clearUserImage;
    self.show_affiliated_terms = show_affiliated_terms;

    function openFriendModal($event) {
      $event.preventDefault();

      $uibModal.open({
        templateUrl: 'friendModal.html',
        controllerAs: 'vm',
        backdrop: 'static',
        controller: ['$uibModalInstance', '$http', 'friendsAPI', controller]
      }).result.then(setFriendID);

      function controller($uibModalInstance, $http, friendsAPI) {
        var self = this;
        self.loadFriends = loadFriends;
        self.page_size = 5;
        self.friends = [];

        self.setSelectedFriend = setSelectedFriend;
        self.close = close;

        function loadFriends(params) {
          self.searchFriendPromise = $http.get(friendsAPI, {params: params});
          self.searchFriendPromise.then(success);

          function success(response) {
            self.friends = response.data;
          }
        }

        function setSelectedFriend(friend) {
          $uibModalInstance.close(friend);
        }

        function close() {
          $uibModalInstance.dismiss();
        }
      }

      function setFriendID(friend) {
        self.selectedFriend = friend;
      }
    }

    function clearSelectedFriend($event) {
      $event.preventDefault();
      self.selectedFriend = {};
    }

    function clearUserImage($event) {
      $event.preventDefault();
      $(':file').filestyle('clear');
    }

    function show_affiliated_terms() {
      self.is_affiliated = false;

      $uibModal.open({
        backdrop: 'static',
        templateUrl: 'affiliated_terms.html',
        controllerAs: 'vm',
        controller: ['$uibModalInstance', controller]
      }).result.then(agree, cancel);

      function controller($uibModalInstance) {
        var self = this;
        self.scroll_down = false;
        self.agree = agree;
        self.cancel = cancel;

        function agree() {
          $uibModalInstance.close();
        }

        function cancel() {
          $uibModalInstance.dismiss();
        }
      }


      function agree() {
        self.is_affiliated = true;
      }

      function cancel() {
        self.is_affiliated = false;
      }
    }
  }
})();
