(function () {
  'use strict';

  angular.module('clickmob').controller('loginCtrl', ['$window', controller]);

  function controller($window) {
    var self = this;

    var login_redirect_url = $window.location.href.split('next=')[1];
    $window.localStorage.login_redirect_url = login_redirect_url || $window.localStorage.login_redirect_url;

    self.next = $window.localStorage.login_redirect_url;
  }
})();
