(function () {
  'use strict';

  angular.module('clickmob')
    .factory('CoreUtils', CoreUtils)
    .factory('CoreUser', ['$resource', 'CoreUtils', CoreUser]);

  function CoreUtils() {
    return {
      setUpResource: setUpResource
    }

    /**
     * Função utilizada para configurar resources junto com o formato de paginação do django rest framework.
     */
    function setUpResource(_Resource, response) {
      var objects = angular.fromJson(response);
      var results = [];
      angular.forEach(objects.results, function (object) {
        results.push(new _Resource(object));
      })
      objects.results = results;
      return objects;
    }
  }

  function CoreUser($resource, CoreUtils) {
    var _CoreUser = $resource('/core/users/', {}, {
      query: {
        method: 'GET',
        isArray: false,
        transformResponse: function (response) {
          return CoreUtils.setUpResource(_CoreUser, response);
        }
      }
    });
    return _CoreUser;
  }
})();
