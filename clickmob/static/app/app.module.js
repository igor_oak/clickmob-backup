(function () {
  'use strict';
  
  angular.module('clickmob', [
    'ui.router',
    'ui.bootstrap',
    'ui.utils.masks',
    'ngResource',
    'cgBusy',
    'uiGmapgoogle-maps',
    'bootstrapLightbox',
    'n3-line-chart',
    'toastr'
  ]);
})();
