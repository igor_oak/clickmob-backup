"""Configuração Heroku wsgi do projeto clickmob."""

import os

from django.core import wsgi
from whitenoise import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "clickmob.settings.heroku")
application = django.DjangoWhiteNoise(wsgi.get_wsgi_application())
