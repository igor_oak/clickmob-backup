"""Módulo contém views genéricas ou globais ao projeto."""

from core import serializers
from django.contrib.auth import mixins
from django.views import generic
from django.conf import settings
from rest_framework import renderers


class IndexView(mixins.LoginRequiredMixin, generic.TemplateView):
    """
    Página base da aplicação.

    Esta página contém a infraestrutura SPA (Single Page Application): scripts, estilos, etc.
    """

    template_name = 'base.html'

    def get_context_data(self, **kwargs):
        """Redefine o contexto da página."""
        context = super().get_context_data(**kwargs)
        user_data = serializers.UserSerializer(self.request.user).data
        if user_data['total_items'] is None:
            user_data['total_items'] = 0

        context['User'] = renderers.JSONRenderer().render(user_data)
        context['GMAPS_KEY'] = settings.GMAPS_KEY
        return context
