"""Modelos da aplicação tickets."""

from django.conf import settings
from django.db import models
from model_utils.models import TimeStampedModel


class Ticket(TimeStampedModel, models.Model):
    """Um ticket aberto pelo usuário."""

    ticket_choices = [
        ('Aberto', 'Aberto'),
        ('Pendente', 'Pendente'),
        ('Em Espera', 'Em Espera'),
        ('Resolvido', 'Resolvido'),
        ('Fechado', 'Fechado'),
    ]

    name = models.CharField(max_length=50)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='tickets')
    description = models.TextField()
    status = models.CharField(max_length=20, choices=ticket_choices, default='Aberto')
    ssn = models.CharField(max_length=18)

    def __str__(self):
        """toString."""
        return '[{} - {}][{}][{}]'.format(
            self.owner.username,
            self.owner.email,
            self.status,
            self.description[:15]
        )

    class Meta:
        ordering = ['-created']
