"""Fábrica de objetos da aplicação tickets."""

import random

import factory

from . import models


class TicketFactory(factory.DjangoModelFactory):
    """Fábrica de tickets."""

    @factory.lazy_attribute
    def name(self):
        """Retorna um nome aleatório."""
        return random.choice(self.names)

    @factory.lazy_attribute
    def description(self):
        """Retorna uma descrição aleatória."""
        return random.choice(self.descriptions)

    @factory.lazy_attribute
    def ssn(self):
        """Retorna um cpf aleatório."""
        return random.choice(self.ssns)

    class Meta:
        model = models.Ticket

    class Params:
        names = ['Igor Carvalho', 'Ismael Panta', 'Igor Oak']
        descriptions = [
            'Estou com problemas na compra de packs.',
            'Estou com problemas para iniciar campanhas.',
            'Estou com problemas para atualizar um ticket.',
            'Estou com problemas para atualizar um ticket.',
            'As visualizações da minha campanha acabam rápido demais.',
        ]
        ssns = [
            '76664375524',
            '39944901709',
            '84182417305',
        ]
