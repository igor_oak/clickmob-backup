"""Comando para criação de tickets aleatórios."""

import os

from django.contrib import auth
from django.core.management import base

from ... import factories


class Command(base.BaseCommand):
    """Cria tickets aleatórios."""

    def add_arguments(self, parser):
        """Adiciona argumentos da linha de comando."""
        parser.add_argument('username')
        parser.add_argument('--total', default=10, type=int)

    def handle(self, **options):
        """Executa o comando."""
        if 'development' in os.environ['DJANGO_SETTINGS_MODULE']:
            username, total = options['username'], options['total']
            owner = auth.get_user_model().objects.get(username=username)
            factories.TicketFactory.create_batch(total, owner=owner)
            print('{} random tickets para o usuário {} foram criados com sucesso.'.format(total, username))
