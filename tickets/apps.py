"""Configuração da aplicação tickets."""

from django.apps import AppConfig


class TicketsConfig(AppConfig):
    """Configura a aplicação."""

    name = 'tickets'
