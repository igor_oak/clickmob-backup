"""Verifica as views da aplicação tickets."""

from django.contrib import auth
from django.core import urlresolvers
from rest_framework import status, test

from .. import models


class TicketViewTest(test.APITestCase):
    """Verifica a API de tickets."""

    @classmethod
    def setUpTestData(cls):
        """Class fixtures."""
        super().setUpTestData()
        cls.User = auth.get_user_model()
        cls.tickets_url = urlresolvers.reverse('tickets:list')
        cls.user1 = cls.User.objects.create_user('user1', 'user1@domain.com', 'pass123')
        cls.user2 = cls.User.objects.create_user('user2', 'user2@domain.com', 'pass123')
        cls.admin = cls.User.objects.create_user('admin', 'admin@domain.com', 'pass123', is_superuser=True)

    def test_create_ticket(self):
        """Verifica a criação de um ticket."""
        self.assertEqual(models.Ticket.objects.count(), 0)

        data = dict(
            name='Razão social',
            description='problema em relação a compra de packs',
            ssn='11111111111',
        )
        self.client.login(username='user1', password='pass123')
        response = self.client.post(self.tickets_url, data=data)

        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(models.Ticket.objects.count(), 1)

        ticket = models.Ticket.objects.get()
        self.assertEqual(ticket.owner, self.user1)
        self.assertEqual(ticket.name, 'Razão social')
        self.assertEqual(ticket.description, 'problema em relação a compra de packs')
        self.assertEqual(ticket.ssn, '11111111111')
        self.assertEqual(ticket.status, 'Aberto')
        self.assertEqual(ticket.get_status_display(), 'Aberto')

    def test_ticket_policies(self):
        """Verifica as restrições de acesso de um ticket."""
        # ticket criado pelo usuário 1
        data = dict(
            name='Razão social',
            description='problema em relação a compra de packs',
            ssn='11111111111',
        )
        self.client.login(username='user1', password='pass123')
        response = self.client.post(self.tickets_url, data)
        user1_ticket_id = response.data['id']

        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(models.Ticket.objects.count(), 1)

        # ticket criado pelo usuário 2
        data = dict(
            name='Razão social2',
            description='problema em relação a compra de packs 2',
            ssn='22222222222',
        )
        self.client.login(username='user2', password='pass123')
        response = self.client.post(self.tickets_url, data)

        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(models.Ticket.objects.count(), 2)
        user2_ticket_id = response.data['id']

        # usuário 1 pode ver seu ticket, junto como admin.
        ticket_url_user1 = urlresolvers.reverse('tickets:detail', args=[user1_ticket_id])

        self.client.login(username='user1', password='pass123')
        response = self.client.get(ticket_url_user1)
        self.assertTrue(status.is_success(response.status_code))

        self.client.login(username='admin', password='pass123')
        response = self.client.get(ticket_url_user1)
        self.assertTrue(status.is_success(response.status_code))

        # mas o usuário 1 não pode ver outros tickets além do seu.
        ticket_url_user2 = urlresolvers.reverse('tickets:detail', args=[user2_ticket_id])
        self.client.login(username='user1', password='pass123')
        response = self.client.get(ticket_url_user2)
        self.assertTrue(status.is_client_error(response.status_code))
