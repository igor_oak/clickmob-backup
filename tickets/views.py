"""Views da aplicação tickets."""

import json

from core.pagination import CorePaginator
from core.permissions import IsOwnerOrAdmin
from django.views import generic
from rest_framework import filters, mixins, permissions, viewsets

from . import models, serializers


class TicketView(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 viewsets.GenericViewSet):
    """Lista ou cria novos tickets via API."""

    serializer_class = serializers.TicketSerializer
    queryset = models.Ticket.objects.all()
    permission_classes = (IsOwnerOrAdmin,)
    pagination_class = CorePaginator
    filter_backends = (filters.SearchFilter,)
    search_fields = ['name', 'status', 'name']

    def get_queryset(self):
        """Obtém a lista de tickets em função do usuário."""
        queryset = super().get_queryset()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(owner=self.request.user)

        return queryset

    def perform_create(self, serializer):
        """Adiciona o usuário logado como dono do recurso."""
        serializer.save(owner=self.request.user)


ticket_list = TicketView.as_view({
    'get': 'list',
    'post': 'create',
})

ticket_detail = TicketView.as_view({
    'get': 'retrieve',
    'put': 'update'
})


class AdminTicketView(viewsets.ModelViewSet):
    """Admin view com todas as permissões."""

    serializer_class = serializers.AdminTicketSerializer
    queryset = models.Ticket.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    pagination_class = CorePaginator
    filter_backends = (filters.SearchFilter,)
    search_fields = ['owner__username', 'owner__email', 'status', 'name', 'ssn']


class SuportTemplate(generic.TemplateView):
    """Página de suporte server-side partial."""

    template_name = 'tickets/suport.html'

    def get_context_data(self, **kwargs):
        """Define dados para o contexto da página."""
        context = super().get_context_data(**kwargs)
        status_options = [dict(value=v, key=k) for k, v in models.Ticket.ticket_choices]
        context['status_options'] = json.dumps(status_options)
        return context


suport_template = SuportTemplate.as_view()
