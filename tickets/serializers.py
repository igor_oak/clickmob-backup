"""Serializadores da aplicação tickets."""

from rest_framework import serializers

from . import models


class TicketSerializer(serializers.ModelSerializer):
    """Serializador de tickets."""

    class Meta:
        model = models.Ticket
        fields = ['id', 'name', 'description', 'created', 'status', 'modified', 'ssn']
        read_only_fields = ['created', 'modified', 'status']


class AdminTicketSerializer(TicketSerializer):
    """Serializador de tickets para o admin."""

    username = serializers.ReadOnlyField(source='owner.username')
    email = serializers.ReadOnlyField(source='owner.email')

    class Meta(TicketSerializer.Meta):
        fields = TicketSerializer.Meta.fields + ['username', 'email']
        read_only_fields = ['created', 'modified']
