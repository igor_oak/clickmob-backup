"""Administração da aplicação tickets."""

from django.contrib import admin

from . import models


@admin.register(models.Ticket)
class TicketAdmin(admin.ModelAdmin):
    """Gerencia tickets pela interface admin."""
