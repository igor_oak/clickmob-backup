"""URLs da aplicação tickets."""

from django.conf import urls

from . import views

urlpatterns = [
    urls.url(r'^list/$', views.ticket_list, name='list'),
    urls.url(r'^list/(?P<pk>\d+)/$', views.ticket_detail, name='detail'),
    urls.url(r'^suport/template/$', views.suport_template, name='suport_template'),
]
